﻿using ServiceStack;
using ServiceStack.Caching;
using Staging_Tool.Backend.DataAccess;
using System;
using System.Linq;

namespace Staging_Tool.Backend.Tools
{
    public class CustomSessionService : Service
    {

        public ICacheClient CacheClient { get; set; }

        public int GetUserId()
        {
            try
            {
                var session = SessionFeature.GetOrCreateSession<AuthUserSession>(CacheClient);
                return Convert.ToInt32(session.UserAuthId);
            }
            catch (Exception e)
            {
                throw e;
            }
        }
        public UserRightIDs GetUserRightsAsIds()
        {
            try
            {
                using (var st = new StagingToolModel())
                {
                    var session = SessionFeature.GetOrCreateSession<AuthUserSession>(CacheClient);
                    int id = Convert.ToInt32(session.UserAuthId);
                    var user = st.st_user.Where(w => w.user_id == id).SingleOrDefault();
                    if (user != null)
                    {
                        if (user.superuser ?? false)
                        {
                            return new UserRightIDs() { admin = true };
                        }
                        var ret = new UserRightIDs();
                        var wardIds = st.st_ward_auth.Where(w => w.user_id == user.user_id).Select(s => s.ward_id_int).ToList();
                        var wardOverDeptIds = st.st_department_auth.Where(w => w.user_id == user.user_id).Join(st.st_department, a => a.department_id, b => b.department_id, (a, b) => new
                        {
                            b.department_id
                        }).Join(st.st_department_ward, a => a.department_id, b => b.department_id, (a, b) => new
                        {
                            b.ward_id_int
                        })
                        .Select(s => s.ward_id_int)
                        .Distinct()
                        .ToList();
                        ret.wardIds = wardIds.Union(wardOverDeptIds).ToList();
                        ret.projectIds = st.st_project_auth.Where(w => w.user_id == user.user_id).Select(s => s.project_id_int).ToList();
                        ret.caseIds = st.st_in_project.Where(w => w.user_id == user.user_id).Select(s => s.case_id_int).ToList();

                        return ret;
                    }
                    else
                    {
                        return new UserRightIDs();
                    }
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

    }
}
