﻿using ServiceStack;
using ServiceStack.Auth;
using ServiceStack.Web;
using Staging_Tool.Backend.DataAccess;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Staging_Tool.Backend.Tools
{
    public class CustomAuthProvider : CredentialsAuthProvider
    {

        public override bool TryAuthenticate(IServiceBase authService,
        string userName, string password)
        {



            try
            {
                using (var st = new StagingToolModel())
                {
                    var user = st.st_user.Where(w => w.user_name == userName && w.user_password == password).SingleOrDefault();

                    if (user != null)
                    {
                        if (user.user_valid_from <= DateTime.Now)
                        {
                            if (!user.user_valid_to.HasValue || user.user_valid_to >= DateTime.Now)
                            {
                                var session = authService.GetSession(false);

                                session.UserAuthId = user.user_id.ToString();

                                session.DisplayName = user.user_name;

                                return true;
                            }
                        }
                    }

                    return false;
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public override IHttpResult OnAuthenticated(IServiceBase authService,
        IAuthSession session, IAuthTokens tokens,
        Dictionary<string, string> authInfo)
        {


            //Fill IAuthSession with data you want to retrieve in the app eg:

            //...

            //Call base method to Save Session and fire Auth/Session callbacks:
            //return base.OnAuthenticated(authService, session, tokens, authInfo);
            authService.SaveSession(session, SessionExpiry);
            return null;
            //Alternatively avoid built-in behavior and explicitly save session with
            //authService.SaveSession(session, SessionExpiry);
            //return null;
        }




    }
}