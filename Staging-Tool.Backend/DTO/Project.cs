﻿using ServiceStack;
using Staging_Tool.Backend.DataAccess;

namespace Staging_Tool.Backend.DTO
{
    [Route("/Project", "POST PUT GET")]
    [Route("/Project/{Id}", "DELETE")]
    public class Project
    {
        public long? Id { get; set; }
        public st_project ProjectObj { get; set; }
    }
}