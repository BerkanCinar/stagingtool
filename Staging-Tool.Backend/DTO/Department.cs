using ServiceStack;
using Staging_Tool.Backend.DataAcessDTO;

namespace Staging_Tool.Backend.DTO
{
    [Route("/Department", "POST PUT GET")]
    [Route("/Department/{Id}", "DELETE")]
    public class Department
    {
        public int? Id { get; set; }
        public Department_DTO department { get; set; }
    }
}
