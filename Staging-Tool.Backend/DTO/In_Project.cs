﻿using ServiceStack;
using Staging_Tool.Backend.DataAccess;
using System.Collections.Generic;

namespace Staging_Tool.Backend.DTO
{
    [Route("/SetPatientToRedCap", "POST")]
    public class SetPatientToRedCap
    {
        public List<PatientInProject> patientDestinationData { get; set; }
    }
}