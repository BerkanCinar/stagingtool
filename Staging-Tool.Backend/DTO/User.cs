using ServiceStack;
using Staging_Tool.Backend.DataAccess;

namespace Staging_Tool.Backend.DTO
{
    [Route("/User", "POST PUT GET")]
    [Route("/User/{Id}", "DELETE")]
    [Route("/User/PasswordReset/{UserId}", "PUT")]
    [Route("/User/ChangePassword", "PUT")]
    [Route("/User/Name/{UserName}", "GET")]

    public class User
    {
        public st_user changePasswordObject { get; set; }
        public string UserName { get; set; }
        public int? Id { get; set; }
        public int? UserId { get; set; }
        public UserRightObjects user { get; set; }
    }

    [Route("/Permission", "GET")]
    public class Permission
    {
    }
}
