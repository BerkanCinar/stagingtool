﻿using ServiceStack;
using Staging_Tool.Backend.DataAccess;
using System;

namespace Staging_Tool.Backend.DTO
{
    [Route("/Case", "POST")]
    public class Case
    {
        public st_hospital_case[] cases { get; set; }
        public Boolean showDischarged { get; set; }
    }

}