﻿using ServiceStack;
using System.Collections.Generic;

namespace Staging_Tool.Backend.DTO
{
    [Route("/PatientInProject", "POST")]
    public class PatientInProjectIDs
    {
        public List<int> caseIDs { get; set; }
        public List<int> projectIDs { get; set; }
    }
}