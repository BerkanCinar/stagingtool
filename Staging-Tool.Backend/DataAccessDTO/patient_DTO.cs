
using System;

namespace Staging_Tool.Backend.DataAcessDTO
{
    public class Patient_DTO
    {
        public int patient_id_int { get; set; }
        public int patient_id_ext { get; set; }
        public string last_name { get; set; }
        public string first_name { get; set; }
        public Nullable<System.DateTime> date_of_birth { get; set; }
        public string gender { get; set; }
        public string street { get; set; }
        public string zip_code { get; set; }
        public string city { get; set; }

        public int alter { get; set; }
        public string date_of_birth_str { get; set; }

    }


}