
using System;

namespace Staging_Tool.Backend.DataAcessDTO
{
    public class User_DTO
    {

        public int user_id { get; set; }
        public string user_name { get; set; }
        public System.DateTime user_valid_from { get; set; }
        public Nullable<System.DateTime> user_valid_to { get; set; }
        public Nullable<bool> superuser { get; set; }


    }


}