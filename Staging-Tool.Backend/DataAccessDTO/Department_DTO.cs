using System.Collections.Generic;

namespace Staging_Tool.Backend.DataAcessDTO
{
    public class Department_DTO
    {
        public int department_id { get; set; }
        public string department_name { get; set; }
        public string department_short_name { get; set; }

        public int case_id_int { get; set; }

        public List<Ward_DTO> wards { get; set; }
    }



}