namespace Staging_Tool.Backend.DataAcessDTO
{
    public class Project_DTO
    {
        public int project_id_int { get; set; }
        public int project_id_ext { get; set; }
        public string project_name { get; set; }
        public string project_admin { get; set; }
        public string token { get; set; }


    }


}