
using System;

namespace Staging_Tool.Backend.DataAcessDTO
{
    public class Hospital_Case_DTO
    {
        public int case_id_int { get; set; }
        public int case_id_ext { get; set; }
        public string case_kind { get; set; }
        public string admission_reason { get; set; }
        public string discharge_reason { get; set; }
        public string referral_doctor { get; set; }
        public string referral_hospital { get; set; }
        public string admission_diagnosis { get; set; }
        public string discharge_diagnosis { get; set; }
        public Nullable<System.DateTime> case_start { get; set; }
        public Nullable<System.DateTime> case_end { get; set; }
        public Nullable<int> patient_id_int { get; set; }
        public Nullable<System.DateTime> case_last_message { get; set; }

        public Patient_DTO patient { get; set; }
        public Project_DTO[] projects { get; set; }
        public Ward_DTO ward_admission { get; set; }
        public Ward_DTO ward_current { get; set; }

        public Department_DTO department_current { get; set; }
        public Department_DTO department_admission { get; set; }

        public string case_start_str { get; set; }
        public string case_last_message_str { get; set; }
        public string projects_str { get; set; }
    }





}