namespace Staging_Tool.Backend.DataAcessDTO
{
    public class Ward_DTO
    {
        public int ward_id_int { get; set; }
        public string ward_id_ext { get; set; }
        public string ward_name { get; set; }
        public string ward_short_name { get; set; }

        public int case_id_int { get; set; }

    }


}