﻿using ServiceStack;
using Staging_Tool.Backend.Tools;
using System.Net;

namespace Staging_Tool.Backend.Services
{

    public class PatientService : CustomSessionService
    {



        [Authenticate]
        public object Post(DTO.PatientInProjectIDs userRequest)
        {
            var user = GetUserRightsAsIds();


            var patientLogic = new BusinessLogic.PatientLogic(GetUserId());

            if (userRequest.projectIDs == null || userRequest.caseIDs == null)
                return new HttpResult(HttpStatusCode.BadRequest, "PARAMETERS ARE MISSING");

            var patients = patientLogic.patientInProject(userRequest.projectIDs, userRequest.caseIDs);

            if (Request.Headers["Accept-Encoding"] != null && Request.Headers["Accept-Encoding"].Contains("gzip"))
            {
                return Request.ToOptimizedResult(patients);
            }

            return patients;
        }
    }
}