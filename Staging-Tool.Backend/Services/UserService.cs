using ServiceStack;
using Staging_Tool.Backend.BusinessLogic;
using Staging_Tool.Backend.DTO;
using Staging_Tool.Backend.Tools;
using System.Net;

namespace Staging_Tool.Backend.Services
{
    public class UserService : CustomSessionService
    {


        [Authenticate]
        public object Get(User userRequest)
        {
            UserLogic ul = new UserLogic();

            if (userRequest.UserName != null)
            {
                var user = ul.GetUserByUserName(userRequest.UserName);

                if (Request.Headers["Accept-Encoding"] != null && Request.Headers["Accept-Encoding"].Contains("gzip"))
                {
                    return Request.ToOptimizedResult(user);
                }

                return user;
            }
            else
            {
                var permission = GetUserRightsAsIds();

                if (!permission.admin) return new HttpResult(HttpStatusCode.Unauthorized, "NOT ALLOWED TO GET USERS");

                var users = ul.GetUser();

                if (Request.Headers["Accept-Encoding"] != null && Request.Headers["Accept-Encoding"].Contains("gzip"))
                {
                    return Request.ToOptimizedResult(users);
                }

                return users;
            }




        }




        [Authenticate]
        public object Post(User userRequest)
        {
            var permission = GetUserRightsAsIds();

            if (!permission.admin) return new HttpResult(HttpStatusCode.Unauthorized, "NOT ALLOWED TO ADD NEW USER");

            UserLogic ul = new UserLogic();

            bool result = ul.modifyUser(userRequest.user);

            if (result)
            {
                return new HttpResult(HttpStatusCode.OK);
            }

            return new HttpResult(HttpStatusCode.InternalServerError, "CAN'T ADD NEW USER");

        }


        [Authenticate]
        public object Delete(User userRequest)
        {
            var permission = GetUserRightsAsIds();

            if (!permission.admin) return new HttpResult(HttpStatusCode.Unauthorized, "NOT ALLOWED TO DELETE USER");

            UserLogic ul = new UserLogic();

            bool result = ul.DeleteUser(userRequest.Id ?? 0);

            if (result)
            {
                return new HttpResult(HttpStatusCode.OK);
            }

            return new HttpResult(HttpStatusCode.InternalServerError, "CAN'T DELETE USER");
        }


        [Authenticate]
        public object Put(User userRequest)
        {


            UserLogic ul = new UserLogic();

            if (userRequest.changePasswordObject != null)
            {

                bool result = ul.changePassword(userRequest.changePasswordObject);

                if (result)
                {
                    return new HttpResult(HttpStatusCode.OK);
                }

                return new HttpResult(HttpStatusCode.InternalServerError, "CAN'T RESET USER PASSWORD");
            }
            else
            {
                var permission = GetUserRightsAsIds();

                if (!permission.admin) return new HttpResult(HttpStatusCode.Unauthorized, "NOT ALLOWED TO RESET USER PASSWORD");

                bool result = ul.resetUserPassword(userRequest.UserId ?? 0);

                if (result)
                {
                    return new HttpResult(HttpStatusCode.OK);
                }

                return new HttpResult(HttpStatusCode.InternalServerError, "CAN'T RESET USER PASSWORD");
            }

        }



    }
}

