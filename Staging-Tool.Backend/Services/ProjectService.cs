﻿using ServiceStack;
using Staging_Tool.Backend.Tools;
using System.Net;

namespace Staging_Tool.Backend.Services
{
    public class ProjectService : CustomSessionService
    {
        [Authenticate]
        public object Get(DTO.Project userRequest)
        {


            var projectLogic = new BusinessLogic.ProjectLogic();

            var projects = projectLogic.getProjects(GetUserRightsAsIds());

            if (Request.Headers["Accept-Encoding"] != null && Request.Headers["Accept-Encoding"].Contains("gzip"))
            {
                return Request.ToOptimizedResult(projects);
            }

            return projects;
        }

        [Authenticate]
        public object Delete(DTO.Project userRequest)
        {

            var projectLogic = new BusinessLogic.ProjectLogic();


            var result = projectLogic.deleteProject(userRequest.Id ?? 0);

            if (result)
            {
                return new HttpResult(HttpStatusCode.OK);
            }

            return new HttpResult(HttpStatusCode.InternalServerError, "CAN'T DELETE PROJECT");
        }

        [Authenticate]
        public object Post(DTO.Project userRequest)
        {
            var projectLogic = new BusinessLogic.ProjectLogic();

            var result = projectLogic.modifyProject(userRequest.ProjectObj);

            if (result)
            {
                return new HttpResult(HttpStatusCode.OK);
            }

            return new HttpResult(HttpStatusCode.InternalServerError, "CAN'T ADD OR MODIFY PROJECT");

        }

        [Authenticate]
        public object Put(DTO.Project userRequest)
        {

            var projectLogic = new BusinessLogic.ProjectLogic();

            var result = projectLogic.modifyProject(userRequest.ProjectObj);

            if (result)
            {
                return new HttpResult(HttpStatusCode.OK);
            }

            return new HttpResult(HttpStatusCode.InternalServerError, "CAN'T ADD OR MODIFY PROJECT");

        }

    }
}