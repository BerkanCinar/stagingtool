﻿using ServiceStack;
using Staging_Tool.Backend.DTO;
using Staging_Tool.Backend.Tools;
using System.Net;

namespace Staging_Tool.Backend.Services
{
    public class In_ProjectService : CustomSessionService
    {
        [Authenticate]
        public object Post(SetPatientToRedCap userRequest)
        {
            var in_ProjectLogic = new BusinessLogic.In_ProjectLogic(GetUserId());

            bool result = in_ProjectLogic.setPatientsToRedCapProject(userRequest.patientDestinationData);

            if (result)
            {
                return new HttpResult(HttpStatusCode.OK);
            }

            return new HttpResult(HttpStatusCode.InternalServerError, "CAN'T ADD NEW DEPARTMENT");
        }
    }
}