﻿using ServiceStack;
using Staging_Tool.Backend.Tools;

namespace Staging_Tool.Backend.Services
{
    public class CaseService : CustomSessionService
    {
        [Authenticate]
        public object Post(DTO.Case userRequest)
        {
            var user = GetUserRightsAsIds();

            var caseLogic = new BusinessLogic.HospitalCaseLogic();

            var cases = caseLogic.getCases(user, userRequest.showDischarged);

            if (Request.Headers["Accept-Encoding"] != null && Request.Headers["Accept-Encoding"].Contains("gzip"))
            {
                return Request.ToOptimizedResult(cases);
            }

            return cases;
        }
    }
}