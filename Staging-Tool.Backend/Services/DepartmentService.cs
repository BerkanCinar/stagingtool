using ServiceStack;
using Staging_Tool.Backend.BusinessLogic;
using Staging_Tool.Backend.DTO;
using Staging_Tool.Backend.Tools;
using System.Net;

namespace Staging_Tool.Backend.Services
{
    public class DepartmentService : CustomSessionService
    {
        [Authenticate]
        public object Post(Department userRequest)
        {
            var permission = GetUserRightsAsIds();

            if (!permission.admin) return new HttpResult(HttpStatusCode.Unauthorized, "NOT ALLOWED TO ADD NEW DEPARTMENTS");

            DepartmentLogic dl = new DepartmentLogic();

            bool result = dl.modifyDepartment(userRequest.department);

            if (result)
            {
                return new HttpResult(HttpStatusCode.OK);
            }

            return new HttpResult(HttpStatusCode.InternalServerError, "CAN'T ADD NEW DEPARTMENT");
        }

        [Authenticate]
        public object Get(Department userRequest)
        {
            var permission = GetUserRightsAsIds();

            if (!permission.admin) return new HttpResult(HttpStatusCode.Unauthorized, "NOT ALLOWED TO GET DEPARTMENTS");

            DepartmentLogic dl = new DepartmentLogic();

            var result = dl.getDepartmentsAndWards();

            if (Request.Headers["Accept-Encoding"] != null && Request.Headers["Accept-Encoding"].Contains("gzip"))
            {
                return Request.ToOptimizedResult(result);
            }

            return result;
        }

        [Authenticate]
        public object Delete(Department userRequest)
        {
            var permission = GetUserRightsAsIds();

            if (!permission.admin) return new HttpResult(HttpStatusCode.Unauthorized, "NOT ALLOWED TO DELETE DEPARTMENT");

            DepartmentLogic dl = new DepartmentLogic();

            bool result = dl.DeleteDepartment(userRequest.Id ?? 0);

            if (result)
            {
                return new HttpResult(HttpStatusCode.OK);
            }

            return new HttpResult(HttpStatusCode.InternalServerError, "CAN'T DELETE DEPARTMENT");
        }
    }
}
