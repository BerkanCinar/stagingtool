using ServiceStack;
using Staging_Tool.Backend.BusinessLogic;
using Staging_Tool.Backend.DTO;
using Staging_Tool.Backend.Tools;

namespace Staging_Tool.Backend.Services
{
    public class PermissionService : CustomSessionService
    {
        [Authenticate]
        public object Get(Permission userRequest)
        {
            PermissionLogic pl = new PermissionLogic();

            var permissions = pl.GetPermissionOptions();

            if (Request.Headers["Accept-Encoding"] != null && Request.Headers["Accept-Encoding"].Contains("gzip"))
            {
                return Request.ToOptimizedResult(permissions);
            }

            return permissions;
        }
    }
}