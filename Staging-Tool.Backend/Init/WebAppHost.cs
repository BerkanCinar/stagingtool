using Funq;
using ServiceStack;
using ServiceStack.Auth;
using ServiceStack.Text;
using Staging_Tool.Backend.Tools;

namespace Staging_Tool.Backend.Init
{
    public class WebAppHost : AppHostBase
    {
        public WebAppHost()
            : base("Staging-Tool Service", typeof(WebAppHost).Assembly)
        {
        }

        public override void Configure(Container container)
        {
            JsConfig.DateHandler = DateHandler.ISO8601;

            Plugins.Add(new AuthFeature(() => new AuthUserSession(),
                new IAuthProvider[] {
                    new CustomAuthProvider()
                }));
        }
    }
}