﻿using System.Collections.Generic;

namespace Staging_Tool.Backend.DataAccess
{
    public class UserRightIDs
    {
        public List<int> projectIds { get; set; }
        public List<int> wardIds { get; set; }
        public List<int> caseIds { get; set; }
        public bool admin { get; set; }
    }
}
