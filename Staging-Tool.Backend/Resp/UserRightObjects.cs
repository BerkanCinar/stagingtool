﻿using Staging_Tool.Backend.DataAcessDTO;
using System.Collections.Generic;

namespace Staging_Tool.Backend.DataAccess
{
    public class UserRightObjects
    {
        public User_DTO user { get; set; }
        public List<Project_DTO> projects { get; set; }
        public List<Ward_DTO> wards { get; set; }
        public List<Hospital_Case_DTO> cases { get; set; }
        public List<Department_DTO> departments { get; set; }
    }
}
