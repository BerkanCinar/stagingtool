﻿using Staging_Tool.Backend.DataAcessDTO;
using System;

namespace Staging_Tool.Backend.DataAccess
{
    public class PatientInProject
    {
        public Project_DTO project { get; set; }
        public Hospital_Case_DTO hospital_case { get; set; }
        public Patient_DTO patient { get; set; }
        public Boolean caseInProject { get; set; }
        public Boolean patientInProject { get; set; }
        public string record_id { get; set; }
        public int reference_id { get; set; }
        public PatientDestination destination { get; set; }
        public Boolean ignore { get; set; }
        public Boolean newRecord { get; set; }
        public Boolean instance { get; set; }
        public Boolean listInstance { get; set; }
    }

    public enum PatientDestination
    {
        NEW = 0,
        INSTANCE = 1,
        IGNORE = 2
    }
}
