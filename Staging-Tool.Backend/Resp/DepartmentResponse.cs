using Staging_Tool.Backend.DataAcessDTO;
using System.Collections.Generic;

namespace Staging_Tool.Backend.Resp
{
    public class DepartmentResponse
    {
        public List<Department_DTO> departments { get; set; }
    }
}