﻿using Staging_Tool.Backend.DataAcessDTO;
using System.Collections.Generic;

namespace Staging_Tool.Backend.DataAccess
{
    public class Permission
    {
        public List<st_project> projects { get; set; }
        public List<st_ward> wards { get; set; }
        public List<Department_DTO> departments { get; set; }
    }
}
