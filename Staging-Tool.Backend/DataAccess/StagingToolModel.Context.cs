﻿//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Staging_Tool.Backend.DataAccess
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class StagingToolModel : DbContext
    {
        public StagingToolModel()
            : base("name=StagingToolModel")
        {
            Configuration.LazyLoadingEnabled = false;
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public DbSet<hl_log> hl_log { get; set; }
        public DbSet<hl_process> hl_process { get; set; }
        public DbSet<st_department> st_department { get; set; }
        public DbSet<st_department_auth> st_department_auth { get; set; }
        public DbSet<st_department_ward> st_department_ward { get; set; }
        public DbSet<st_handled_on> st_handled_on { get; set; }
        public DbSet<st_hospital_case> st_hospital_case { get; set; }
        public DbSet<st_in_project> st_in_project { get; set; }
        public DbSet<st_patient> st_patient { get; set; }
        public DbSet<st_project> st_project { get; set; }
        public DbSet<st_project_auth> st_project_auth { get; set; }
        public DbSet<st_staging_tool_config> st_staging_tool_config { get; set; }
        public DbSet<st_user> st_user { get; set; }
        public DbSet<st_ward> st_ward { get; set; }
        public DbSet<st_ward_auth> st_ward_auth { get; set; }
    }
}
