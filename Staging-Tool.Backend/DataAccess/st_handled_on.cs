//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Staging_Tool.Backend.DataAccess
{
    using System;
    using System.Collections.Generic;
    
    public partial class st_handled_on
    {
        public int handled_id { get; set; }
        public int case_id_int { get; set; }
        public int ward_id_int { get; set; }
        public string room { get; set; }
        public string bed { get; set; }
        public Nullable<System.DateTime> transfer_date { get; set; }
        public string transfer_kind { get; set; }
        public Nullable<int> department_id { get; set; }
    
        public virtual st_department st_department { get; set; }
        public virtual st_hospital_case st_hospital_case { get; set; }
        public virtual st_ward st_ward { get; set; }
    }
}
