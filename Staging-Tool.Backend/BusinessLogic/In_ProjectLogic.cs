﻿using Newtonsoft.Json;
using ServiceStack;
using Staging_Tool.Backend.DataAccess;
using Staging_Tool.Backend.DataAcessDTO;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace Staging_Tool.Backend.BusinessLogic
{
    public class In_ProjectLogic
    {

        private readonly int userId;
        private readonly string dateFormat;
        private readonly string timeFormat;

        public In_ProjectLogic(int id)
        {
            userId = id;
            dateFormat = "yyyy-MM-dd";
            timeFormat = "HH:mm";
        }

        public Boolean setPatientsToRedCapProject(List<PatientInProject> patientDestinationData)
        {
            try
            {


                string url = ConfigurationManager.AppSettings["redcapurl"];

                if (url == null) return false;

                using (var st = new StagingToolModel())
                {
                    Boolean ret = true;
                    patientDestinationData = patientDestinationData.OrderBy(o => o.reference_id).ThenBy(o => o.destination).ToList();

                    foreach (var data in patientDestinationData)
                    {
                        string token = st.st_project.FirstOrDefault(w => w.project_id_int == data.project.project_id_int).token;

                        switch (data.destination)
                        {
                            case PatientDestination.NEW:

                                var wards = st.st_handled_on.Where(w => w.case_id_int == data.hospital_case.case_id_int).Join(st.st_ward, a => a.ward_id_int, b => b.ward_id_int, (a, b) => new
                                {
                                    b.ward_id_int,
                                    b.ward_id_ext,
                                    a.department_id,
                                    b.ward_name,
                                    b.ward_short_name,

                                    a.transfer_date,
                                    a.room,
                                    a.bed,
                                    a.transfer_kind
                                }).OrderBy(o => o.transfer_date).Select(s => new WardHandledOn()
                                {
                                    bed = s.bed,
                                    transfer_kind = s.transfer_kind,
                                    room = s.room,
                                    transfer = s.transfer_date,
                                    pflege = s.ward_short_name,
                                    pflege_bez = s.ward_name,
                                    oe = st.st_department.Where(w => w.department_id == s.department_id).FirstOrDefault().department_short_name,
                                    oe_bez = st.st_department.Where(w => w.department_id == s.department_id).FirstOrDefault().department_name,
                                }).ToList();



                                ret = ret && NewRecord(url, token, data.patient, data.hospital_case, wards, data.project);

                                // refresh recordIDs of following instances
                                var newRecordId = st.st_in_project.Where(w => w.case_id_int == data.hospital_case.case_id_int && w.project_id_int == data.project.project_id_int).FirstOrDefault().record_id;

                                if (newRecordId != null)
                                {
                                    foreach (var el in patientDestinationData)
                                    {
                                        if (data.reference_id == el.reference_id)
                                        {
                                            el.record_id = newRecordId;
                                        }
                                    }
                                }
                                break;
                            case PatientDestination.INSTANCE:
                                var wardsForInstance = st.st_handled_on.Where(w => w.case_id_int == data.hospital_case.case_id_int).Join(st.st_ward, a => a.ward_id_int, b => b.ward_id_int, (a, b) => new
                                {
                                    b.ward_id_int,
                                    b.ward_id_ext,
                                    a.department_id,
                                    b.ward_name,
                                    b.ward_short_name,


                                    a.transfer_date,
                                    a.room,
                                    a.bed,
                                    a.transfer_kind
                                }).OrderBy(o => o.transfer_date).Select(s => new WardHandledOn()
                                {
                                    bed = s.bed,
                                    transfer_kind = s.transfer_kind,
                                    room = s.room,
                                    transfer = s.transfer_date,
                                    pflege = s.ward_short_name,
                                    pflege_bez = s.ward_name,
                                    oe = st.st_department.Where(w => w.department_id == s.department_id).FirstOrDefault().department_short_name,
                                    oe_bez = st.st_department.Where(w => w.department_id == s.department_id).FirstOrDefault().department_name,
                                }).ToList();


                                // AOA aktualisierung
                                var patInProject = st.st_in_project.Where(w => w.record_id == data.record_id).FirstOrDefault();
                                bool refreshAOA = false;
                                if (patInProject != null)
                                {
                                    var latestHandledOnObj = st.st_handled_on.Where(w => w.case_id_int == patInProject.case_id_int).OrderBy(o => o.transfer_date).ToList().LastOrDefault();
                                    var aoaTransferDate = wardsForInstance.LastOrDefault();
                                    if (aoaTransferDate != null && latestHandledOnObj != null)
                                    {
                                        if (latestHandledOnObj.transfer_date < aoaTransferDate.transfer)
                                        {
                                            refreshAOA = true;
                                        }
                                    }
                                }

                                ret = ret && NewFallInstance(url, token, data.hospital_case, data.record_id, data.project.project_id_int, wardsForInstance, data.patient, refreshAOA);
                                break;
                            case PatientDestination.IGNORE:
                                ret = ret && true;
                                break;
                            default:
                                ret = false;
                                break;

                        }

                    }
                    return ret;
                }

            }
            catch (Exception e)
            {
                return false;
                throw e;
            }

        }

        public Boolean AddRecordToStagingTool(int caseId, int projectId, int userId, string record_id)
        {
            try
            {
                using (var st = new StagingToolModel())
                {
                    st_in_project ip = new st_in_project();
                    ip.case_id_int = caseId;
                    ip.project_id_int = projectId;
                    ip.user_id = userId;
                    ip.record_id = record_id;
                    ip.project_start = DateTime.Now;

                    st.st_in_project.Add(ip);
                    st.SaveChanges();
                }

                return true;
            }
            catch
            {
                return false;
            }

        }

        public void DeleteRecordFromStagingTool(int caseId, int projectId)
        {
            try
            {
                using (var st = new StagingToolModel())
                {
                    var result = (from ip in st.st_in_project
                                  where ip.case_id_int == caseId
                                  && ip.project_id_int == projectId
                                  select ip).FirstOrDefault();

                    if (result != null)
                    {
                        st.st_in_project.Remove(result);
                        st.SaveChanges();
                    }
                }
            }
            catch
            {
                // ja, was machen wir denn hiermit....               --> war kein Logging gewünscht aber wäre erstmal nen FATAL ERROR! und würde zu problemen führen 
            }
        }
        private int getAge(DateTime? dateOfBirth)
        {
            if (dateOfBirth != null)
            {
                int ret = DateTime.Now.Year - dateOfBirth.Value.Year;
                if (dateOfBirth.Value.Month > DateTime.Now.Month || DateTime.Now.Month == dateOfBirth.Value.Month && DateTime.Now.Day < dateOfBirth.Value.Day)
                {
                    ret--;
                }
                return ret;
            }
            return 0;
        }


        public Boolean NewRecord(string url, string token, Patient_DTO patient, Hospital_Case_DTO hospital_case, List<WardHandledOn> wards, Project_DTO project)
        {
            try
            {
                // Get next Record ID
                string paramQuery = "token=" + token + "&content=generateNextRecordName";
                var newID = url.PostToUrl(paramQuery);


                var dataObjectForRedCap = new List<Record>();

                // Stammdaten pflegen
                dataObjectForRedCap.Add(new Record
                {

                    // Stammdaten
                    record_id = newID,
                    sd_patnummer = patient.patient_id_ext.ToString(),
                    sd_vorname = patient.first_name,
                    sd_name = patient.last_name,
                    sd_geburtstag = patient.date_of_birth,
                    sd_geschlecht = patient.gender,
                    stammdaten_complete = "2",

                    // CASE
                    fd_fallnummer = hospital_case.case_id_ext.ToString(),
                    fd_fallart = hospital_case.case_kind,
                    fd_auf_grund = hospital_case.admission_reason,

                    fd_auf_diagnose = hospital_case.admission_diagnosis,
                    fd_auf_datum = hospital_case.case_start?.ToString(dateFormat),
                    fd_auf_zeit = hospital_case.case_start?.ToString(timeFormat),
                    fd_ent_grund = hospital_case.case_end != null ? hospital_case.discharge_reason : null,
                    fd_ent_diagnose = hospital_case.case_end != null ? hospital_case.discharge_diagnosis : null,
                    fd_ent_datum = hospital_case.case_end?.ToString(dateFormat),
                    fd_ent_zeit = hospital_case.case_end?.ToString(timeFormat),

                    fd_auf_oe = wards.FirstOrDefault()?.oe,
                    fd_auf_oe_bez = wards.FirstOrDefault()?.oe_bez,

                    fd_auf_pflege = wards.FirstOrDefault()?.pflege,
                    fd_auf_pflege_bez = wards.FirstOrDefault()?.pflege_bez,
                    fd_auf_zimmer = wards.FirstOrDefault()?.room,
                    fd_auf_bett = wards.FirstOrDefault()?.bed,

                    fd_ent_oe = hospital_case.case_end != null ? wards.LastOrDefault()?.oe : null,
                    fd_ent_oe_bez = hospital_case.case_end != null ? wards.LastOrDefault()?.oe_bez : null,
                    fd_ent_pflege = hospital_case.case_end != null ? wards.LastOrDefault()?.pflege : null,
                    fd_ent_pflege_bez = hospital_case.case_end != null ? wards.LastOrDefault()?.pflege_bez : null,

                    fd_ent_zimmer = wards.LastOrDefault()?.room,
                    fd_ent_bett = wards.LastOrDefault()?.bed,
                    fd_alter = getAge(patient.date_of_birth).ToString(),
                    falldaten_complete = "2",

                    // AOA
                    aoa_fallnummer = hospital_case.case_id_ext.ToString(),
                    aoa_fallstatus = hospital_case.case_end != null ? "0" : "1",
                    aoa_auf_datum = hospital_case.case_start?.ToString(dateFormat),
                    aoa_auf_zeit = hospital_case.case_start?.ToString(timeFormat),
                    aoa_auf_oe = wards.FirstOrDefault()?.oe,
                    aoa_auf_oe_bez = wards.FirstOrDefault()?.oe_bez,
                    aoa_auf_pflege = wards.FirstOrDefault()?.pflege,
                    aoa_auf_pflege_bez = wards.FirstOrDefault()?.pflege_bez,
                    aoa_auf_zimmer = wards.FirstOrDefault()?.room,
                    aoa_auf_bett = wards.FirstOrDefault()?.bed,

                    // AOA VER

                    aoa_ver_datum = (wards.Count >= 2) ? wards.LastOrDefault()?.transfer?.ToString(dateFormat) : null,
                    aoa_ver_zeit = (wards.Count >= 2) ? wards.LastOrDefault()?.transfer?.ToString(timeFormat) : null,
                    aoa_ver_oe = (wards.Count >= 2) ? wards.LastOrDefault()?.oe : null,
                    aoa_ver_oe_bez = (wards.Count >= 2) ? wards.LastOrDefault()?.oe_bez : null,
                    aoa_ver_pflege = (wards.Count >= 2) ? wards.LastOrDefault()?.pflege : null,
                    aoa_ver_pflege_bez = (wards.Count >= 2) ? wards.LastOrDefault()?.pflege_bez : null,
                    aoa_ver_zimmer = (wards.Count >= 2) ? wards.LastOrDefault()?.room : null,
                    aoa_ver_bett = (wards.Count >= 2) ? wards.LastOrDefault()?.bed : null,
                    //aoa_ver_datum = hospital_case.case_end?.ToString(dateFormat),
                    //aoa_ver_zeit = hospital_case.case_end?.ToString(timeFormat),
                    //aoa_ver_oe = hospital_case.case_end != null ? wards.LastOrDefault()?.oe : null,
                    //aoa_ver_oe_bez = hospital_case.case_end != null ? wards.LastOrDefault()?.oe_bez : null,
                    //aoa_ver_pflege = hospital_case.case_end != null ? wards.LastOrDefault()?.pflege : null,
                    //aoa_ver_pflege_bez = hospital_case.case_end != null ? wards.LastOrDefault()?.pflege_bez : null,

                    aufenthaltsort_aktuell_complete = "2"
                });

                // Stationen einordnen
                for (int i = 0; i < wards.Count; i++)
                {
                    var tmp = new Record()
                    {
                        record_id = newID,

                        aoh_fallnummer = hospital_case.case_id_ext.ToString(),
                        aoh_ver_datum = wards[i].transfer?.ToString(dateFormat),
                        aoh_ver_zeit = wards[i].transfer?.ToString(timeFormat),

                        aoh_ver_oe = wards[i]?.oe,
                        aoh_ver_oe_bez = wards[i]?.oe_bez,
                        aoh_ver_pflege = wards[i]?.pflege,
                        aoh_ver_pflege_bez = wards[i]?.pflege_bez,


                        aoh_ver_zimmer = wards[i].room,
                        aoh_ver_bett = wards[i].bed,
                        aufenthaltsort_historie_complete = "2",

                        redcap_repeat_instrument = "aufenthaltsort_historie",
                        redcap_repeat_instance = (i + 1).ToString()
                    };

                    dataObjectForRedCap.Add(tmp);
                }


                Boolean addToStagingToolSuccess = AddRecordToStagingTool(hospital_case.case_id_int, project.project_id_int, userId, newID);



                if (addToStagingToolSuccess)
                {
                    // Objekt als json serializen
                    var jsonData = JsonConvert.SerializeObject(dataObjectForRedCap, new JsonSerializerSettings
                    {
                        DateFormatString = "yyy-MM-dd",
                        NullValueHandling = NullValueHandling.Ignore
                    });

                    paramQuery = "token=" + token + "&content=record&format=json&type=flat&overwriteBehavior=normal&data=" + jsonData + "";

                    // Request abschicken
                    var resp = url.PostToUrl(paramQuery);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                DeleteRecordFromStagingTool(hospital_case.case_id_int, project.project_id_int);
                return false;
            }

        }


        public Boolean NewFallInstance(string url, string token, Hospital_Case_DTO hospital_case, string _record_id, int projectId, List<WardHandledOn> wards, Patient_DTO patient, bool refreshAOA)
        {
            try
            {
                // Get Instances of Fälle
                string form = "falldaten";
                string paramQuery = "token=" + token + "&" +
                    "content=record" +
                    "&format=json" +
                    "&type=flat" +
                    "&forms=" + form +
                     "&records=" + _record_id;

                var currentInstances = url.PostToUrl(paramQuery);
                // Set New Instance
                var currInstanceObject = JsonConvert.DeserializeObject<List<Record>>(currentInstances, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });


                // Add new Instance
                var dataObjectForRedCap = new List<Record>();
                dataObjectForRedCap.Add(new Record
                {

                    record_id = _record_id,

                    // CASE
                    fd_fallnummer = hospital_case.case_id_ext.ToString(),
                    fd_fallart = hospital_case.case_kind,
                    fd_auf_grund = hospital_case.admission_reason,

                    fd_auf_diagnose = hospital_case.admission_diagnosis,
                    fd_auf_datum = hospital_case.case_start?.ToString(dateFormat),
                    fd_auf_zeit = hospital_case.case_start?.ToString(timeFormat),
                    fd_ent_grund = hospital_case.case_end != null ? hospital_case.discharge_reason : null,
                    fd_ent_diagnose = hospital_case.case_end != null ? hospital_case.discharge_diagnosis : null,
                    fd_ent_datum = hospital_case.case_end?.ToString(dateFormat),
                    fd_ent_zeit = hospital_case.case_end?.ToString(timeFormat),

                    fd_auf_oe = wards.FirstOrDefault()?.oe,
                    fd_auf_oe_bez = wards.FirstOrDefault()?.oe_bez,
                    fd_auf_pflege = wards.FirstOrDefault()?.pflege,
                    fd_auf_pflege_bez = wards.FirstOrDefault()?.pflege_bez,


                    fd_auf_zimmer = wards.FirstOrDefault()?.room,
                    fd_auf_bett = wards.FirstOrDefault()?.bed,


                    fd_ent_oe = hospital_case.case_end != null ? wards.LastOrDefault()?.oe : null,
                    fd_ent_oe_bez = hospital_case.case_end != null ? wards.LastOrDefault()?.oe_bez : null,

                    fd_ent_pflege = hospital_case.case_end != null ? wards.LastOrDefault()?.pflege : null,
                    fd_ent_pflege_bez = hospital_case.case_end != null ? wards.LastOrDefault()?.pflege_bez : null,

                    fd_ent_zimmer = wards.LastOrDefault()?.room,
                    fd_ent_bett = wards.LastOrDefault()?.bed,
                    fd_alter = getAge(patient.date_of_birth).ToString(),
                    falldaten_complete = "2",

                    redcap_repeat_instrument = "falldaten",
                    redcap_repeat_instance = (currInstanceObject.Count() + 1).ToString()
                });



                // +++++++++++++++++++++++++++++++++++++++++++++
                // +++++ Stationen rückwirkend nachfüllen! +++++
                // +++++++++++++++++++++++++++++++++++++++++++++

                form = "aufenthaltsort_historie";
                paramQuery = "token=" + token + "&" +
                    "content=record" +
                    "&format=json" +
                    "&type=flat" +
                    "&forms=" + form +
                     "&records=" + _record_id;

                var currentWardInstances = url.PostToUrl(paramQuery);

                var currWardInstanceObject = JsonConvert.DeserializeObject<List<Record>>(currentInstances, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });

                // Stationen einordnen
                for (int i = 0; i < wards.Count; i++)
                {
                    var tmp = new Record()
                    {
                        record_id = _record_id,

                        aoh_fallnummer = hospital_case.case_id_ext.ToString(),
                        aoh_ver_datum = wards[i].transfer?.ToString(dateFormat),
                        aoh_ver_zeit = wards[i].transfer?.ToString(timeFormat),

                        aoh_ver_oe = wards[i]?.oe,
                        aoh_ver_oe_bez = wards[i]?.oe_bez,
                        aoh_ver_pflege = wards[i]?.pflege,
                        aoh_ver_pflege_bez = wards[i]?.pflege_bez,

                        aoh_ver_zimmer = wards[i].room,
                        aoh_ver_bett = wards[i].bed,
                        aufenthaltsort_historie_complete = "2",

                        redcap_repeat_instrument = "aufenthaltsort_historie",
                        redcap_repeat_instance = (currWardInstanceObject.Count() + (1 + i)).ToString()
                    };

                    dataObjectForRedCap.Add(tmp);
                }

                if (refreshAOA)
                {
                    dataObjectForRedCap.Add(new Record
                    {
                        record_id = _record_id,

                        aoa_fallnummer = hospital_case.case_id_ext.ToString(),
                        aoa_fallstatus = hospital_case.case_end != null ? "0" : "1",
                        aoa_auf_datum = hospital_case.case_start?.ToString(dateFormat),
                        aoa_auf_zeit = hospital_case.case_start?.ToString(timeFormat),

                        aoa_auf_oe = wards.FirstOrDefault()?.oe,
                        aoa_auf_oe_bez = wards.FirstOrDefault()?.oe_bez,
                        aoa_auf_pflege = wards.FirstOrDefault()?.pflege,
                        aoa_auf_pflege_bez = wards.FirstOrDefault()?.pflege_bez,


                        aoa_auf_zimmer = wards.FirstOrDefault()?.room,
                        aoa_auf_bett = wards.FirstOrDefault()?.bed,

                        // AOA VER

                        aoa_ver_datum = (wards.Count >= 2) ? wards.LastOrDefault()?.transfer?.ToString(dateFormat) : null,
                        aoa_ver_zeit = (wards.Count >= 2) ? wards.LastOrDefault()?.transfer?.ToString(timeFormat) : null,
                        aoa_ver_oe = (wards.Count >= 2) ? wards.LastOrDefault()?.oe : null,
                        aoa_ver_oe_bez = (wards.Count >= 2) ? wards.LastOrDefault()?.oe_bez : null,
                        aoa_ver_pflege = (wards.Count >= 2) ? wards.LastOrDefault()?.pflege : null,
                        aoa_ver_pflege_bez = (wards.Count >= 2) ? wards.LastOrDefault()?.pflege_bez : null,
                        aoa_ver_zimmer = (wards.Count >= 2) ? wards.LastOrDefault()?.room : null,
                        aoa_ver_bett = (wards.Count >= 2) ? wards.LastOrDefault()?.bed : null,
                    });
                }


                var jsonData = JsonConvert.SerializeObject(dataObjectForRedCap, new JsonSerializerSettings
                {
                    DateFormatString = "yyy-MM-dd",
                    NullValueHandling = NullValueHandling.Ignore
                });

                paramQuery = "token=" + token + "&content=record&format=json&type=flat&overwriteBehavior=normal&data=" + jsonData + "";

                Boolean succ = AddRecordToStagingTool(hospital_case.case_id_int, projectId, userId, _record_id);

                if (succ)
                {
                    // Request abschicken
                    var resp = url.PostToUrl(paramQuery);
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {
                DeleteRecordFromStagingTool(hospital_case.case_id_int, projectId);
                return false;
            }

        }


        public class Record
        {

            public string redcap_repeat_instance { get; set; }
            public string redcap_repeat_instrument { get; set; }
            // Stammdaten
            public string record_id { get; set; }
            public string sd_patnummer { get; set; }
            public string sd_name { get; set; }
            public string sd_vorname { get; set; }
            public DateTime? sd_geburtstag { get; set; }
            public string sd_geschlecht { get; set; }
            public string sd_notiz { get; set; }
            public string stammdaten_complete { get; set; }


            // Falldaten

            public string fd_fallnummer { get; set; }
            public string fd_fallart { get; set; }
            public string fd_auf_grund { get; set; }
            public string fd_auf_diagnose { get; set; }
            public string fd_auf_datum { get; set; }
            public string fd_auf_zeit { get; set; }
            public string fd_ent_grund { get; set; }
            public string fd_ent_diagnose { get; set; }
            public string fd_ent_datum { get; set; }
            public string fd_ent_zeit { get; set; }
            public string fd_auf_oe { get; set; }
            public string fd_auf_oe_bez { get; set; }
            public string fd_auf_pflege { get; set; }
            public string fd_auf_pflege_bez { get; set; }
            public string fd_auf_zimmer { get; set; }
            public string fd_auf_bett { get; set; }
            public string fd_ent_oe { get; set; }
            public string fd_ent_oe_bez { get; set; }
            public string fd_ent_pflege { get; set; }
            public string fd_ent_pflege_bez { get; set; }
            public string fd_ent_zimmer { get; set; }
            public string fd_ent_bett { get; set; }
            public string fd_alter { get; set; }
            public string fd_notiz { get; set; }
            public string falldaten_complete { get; set; }



            // Aufenthalt
            public string aoh_fallnummer { get; set; }
            public string aoh_ver_datum { get; set; }
            public string aoh_ver_zeit { get; set; }
            public string aoh_ver_oe { get; set; }
            public string aoh_ver_oe_bez { get; set; }
            public string aoh_ver_pflege { get; set; }
            public string aoh_ver_pflege_bez { get; set; }
            public string aoh_ver_zimmer { get; set; }
            public string aoh_ver_bett { get; set; }
            public string aufenthaltsort_historie_complete { get; set; }

            // Aufenthalt aktuell

            public string aoa_fallnummer { get; set; }
            public string aoa_fallstatus { get; set; }
            public string aoa_auf_datum { get; set; }
            public string aoa_auf_zeit { get; set; }
            public string aoa_auf_oe { get; set; }
            public string aoa_auf_oe_bez { get; set; }
            public string aoa_auf_pflege { get; set; }
            public string aoa_auf_pflege_bez { get; set; }
            public string aoa_auf_zimmer { get; set; }
            public string aoa_auf_bett { get; set; }
            public string aoa_ver_datum { get; set; }
            public string aoa_ver_zeit { get; set; }
            public string aoa_ver_oe { get; set; }
            public string aoa_ver_oe_bez { get; set; }
            public string aoa_ver_pflege { get; set; }
            public string aoa_ver_pflege_bez { get; set; }
            public string aoa_ver_zimmer { get; set; }
            public string aoa_ver_bett { get; set; }
            public string aufenthaltsort_aktuell_complete { get; set; }

        }

        public class WardHandledOn
        {
            public string room { get; set; }
            public string bed { get; set; }
            public string transfer_kind { get; set; }
            public DateTime? transfer { get; set; }
            public string oe { get; set; }
            public string oe_bez { get; set; }
            public string pflege { get; set; }
            public string pflege_bez { get; set; }

        }

    }
}