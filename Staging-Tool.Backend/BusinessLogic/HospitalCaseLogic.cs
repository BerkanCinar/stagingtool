﻿using Staging_Tool.Backend.DataAccess;
using Staging_Tool.Backend.DataAcessDTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Staging_Tool.Backend.BusinessLogic
{
    public class HospitalCaseLogic
    {
        public List<Hospital_Case_DTO> getCases(DataAccess.UserRightIDs user, Boolean showDischarged)
        {

            try
            {
                using (var st = new StagingToolModel())
                {

                    List<int> caseIds;
                    if (!user.admin)
                    {
                        // Select alle Cases auf die Der Nutzer Zugriff hat
                        caseIds = (//Alle Fälle die dem Nutzer zugeordnet sind
                                      from s in st.st_hospital_case
                                      where user.caseIds.Contains(s.case_id_int)
                                      select s.case_id_int)
                                      .Concat(
                                      //Alle Fälle die in Projekten sind, dessen Berechtiung der Nutzer hat
                                      from ip in st.st_in_project
                                      where user.projectIds.Contains(ip.project_id_int)
                                      select ip.case_id_int)
                                      .Concat(
                                      // Alle Fälle von Stationen, auf die der Nutzer Zugriff hat
                                      from ho in st.st_handled_on
                                      where user.wardIds.Contains(ho.ward_id_int)
                                      && ho.transfer_date == (from hh in st.st_handled_on
                                                              where hh.case_id_int == ho.case_id_int
                                                              select hh.transfer_date).Max()
                                      select ho.case_id_int).Distinct().ToList();
                    }
                    else
                    {
                        // Select alle Cases
                        caseIds = (from s in st.st_hospital_case
                                   select s.case_id_int).ToList();
                    }



                    // Select alle Cases auf Basis der CaseIds
                    DateTime nowMinus28Days = DateTime.Now.AddDays(-28);
                    List<Hospital_Case_DTO> cases;
                    if (showDischarged == false)
                    {
                        cases = (from s in st.st_hospital_case
                                 where caseIds.Contains(s.case_id_int)
                                 && (s.case_end == null && s.case_kind.ToLower() != "andere")
                                 || (s.case_end == null && s.case_kind.ToLower() == "andere" && s.case_start >= nowMinus28Days)
                                 select new Hospital_Case_DTO()
                                 {
                                     case_id_int = s.case_id_int,
                                     case_id_ext = s.case_id_ext,
                                     case_kind = s.case_kind,
                                     admission_reason = s.admission_reason,
                                     discharge_reason = s.discharge_reason,
                                     referral_doctor = s.referral_doctor,
                                     referral_hospital = s.referral_hospital,
                                     admission_diagnosis = s.admission_diagnosis,
                                     discharge_diagnosis = s.discharge_diagnosis,
                                     case_start = s.case_start,
                                     case_end = s.case_end,
                                     patient_id_int = s.patient_id_int,
                                     case_last_message = s.case_last_message,
                                 }).ToList();
                    }
                    else
                    {
                        cases = (from s in st.st_hospital_case
                                 where caseIds.Contains(s.case_id_int)
                                 && s.case_end != null
                                 || (s.case_end == null && s.case_kind.ToLower() == "andere" && s.case_start < nowMinus28Days)
                                 select new Hospital_Case_DTO()
                                 {
                                     case_id_int = s.case_id_int,
                                     case_id_ext = s.case_id_ext,
                                     case_kind = s.case_kind,
                                     admission_reason = s.admission_reason,
                                     discharge_reason = s.discharge_reason,
                                     referral_doctor = s.referral_doctor,
                                     referral_hospital = s.referral_hospital,
                                     admission_diagnosis = s.admission_diagnosis,
                                     discharge_diagnosis = s.discharge_diagnosis,
                                     case_start = s.case_start,
                                     case_end = s.case_end,
                                     patient_id_int = s.patient_id_int,
                                 }).ToList();
                    }




                    //gleiche wie oben, nur in einem Statement...

                    //var selectResult = (//Alle Fälle die dem Nutzer zugeordnet sind
                    //                    from s in st.st_hospital_case
                    //                    where user.caseIds.Contains(s.case_id_int)
                    //                    && s.case_end == null
                    //                    select new Hospital_Case_DTO()
                    //                    {
                    //                        case_id_int = s.case_id_int
                    //                    }).Concat(
                    //                    //Alle Fälle die in Projekten sind, dessen Berechtiung der Nutzer hat
                    //                    from s in st.st_hospital_case
                    //                    where (from ip in st.st_in_project
                    //                           where user.projectIds.Contains(ip.project_id_int)
                    //                           select ip.case_id_int).Distinct().ToList().Contains(s.case_id_int)
                    //                    && s.case_end == null
                    //                    select new Hospital_Case_DTO()
                    //                    {
                    //                        case_id_int = s.case_id_int
                    //                    }).Concat(
                    //                    // Alle Fälle von Stationen, auf die der Nutzer Zugriff hat    
                    //                    from s in st.st_hospital_case
                    //                    where (from ho in st.st_handled_on
                    //                           where user.wardIds.Contains(ho.ward_id_int)
                    //                           && ho.transfer_date == (from hh in st.st_handled_on
                    //                                                   where hh.case_id_int == ho.case_id_int
                    //                                                   select hh.transfer_date).Max()
                    //                           select ho.case_id_int).Distinct().ToList().Contains(s.case_id_int)
                    //                    && s.case_end == null
                    //                    select new Hospital_Case_DTO()
                    //                    {
                    //                        case_id_int = s.case_id_int
                    //                    }).Distinct().ToList();




                    // select Patientendaten zu den Cases
                    var patients = (from p in st.st_patient
                                    join c in st.st_hospital_case
                                    on p.patient_id_int equals c.patient_id_int
                                    where caseIds.Contains(c.case_id_int)
                                    select new Patient_DTO
                                    {
                                        patient_id_int = p.patient_id_int,
                                        patient_id_ext = p.patient_id_ext,
                                        last_name = p.last_name,
                                        first_name = p.first_name,
                                        date_of_birth = p.date_of_birth,
                                        gender = p.gender,
                                        street = p.street,
                                        zip_code = p.zip_code,
                                        city = p.city,
                                    }).Distinct().ToList().ToDictionary(x => x.patient_id_int);



                    // select Aufnahme Station
                    var wards_admission = (from w in st.st_ward
                                           join ho in st.st_handled_on
                                           on w.ward_id_int equals ho.ward_id_int
                                           where caseIds.Contains(ho.case_id_int)
                                           && ho.handled_id == (from hh in st.st_handled_on
                                                                   where hh.case_id_int == ho.case_id_int
                                                                   select hh.handled_id).Min()
                                           select new Ward_DTO
                                           {
                                               ward_id_int = w.ward_id_int,
                                               ward_id_ext = w.ward_id_ext,
                                               ward_name = w.ward_name,
                                               ward_short_name = w.ward_short_name,
                                               case_id_int = ho.case_id_int
                                           }).ToList().ToDictionary(x => x.case_id_int);


                    // select aktuelle Station
                    var wards_current = (from w in st.st_ward
                                         join ho in st.st_handled_on
                                         on w.ward_id_int equals ho.ward_id_int
                                         where caseIds.Contains(ho.case_id_int)
                                         && ho.handled_id == (from hh in st.st_handled_on
                                                                 where hh.case_id_int == ho.case_id_int
                                                                 select hh.handled_id).Max()
                                         select new Ward_DTO
                                         {
                                             ward_id_int = w.ward_id_int,
                                             ward_id_ext = w.ward_id_ext,
                                             ward_name = w.ward_name,
                                             ward_short_name = w.ward_short_name,
                                             case_id_int = ho.case_id_int
                                         }).ToList().ToDictionary(x => x.case_id_int);


                    // select Aufnahme Station
                    var departments_admission = (from d in st.st_department
                                                 join ho in st.st_handled_on
                                                 on d.department_id equals ho.department_id
                                                 where caseIds.Contains(ho.case_id_int)
                                                 && ho.handled_id == (from hh in st.st_handled_on
                                                                         where hh.case_id_int == ho.case_id_int
                                                                         select hh.handled_id).Min()
                                                 select new Department_DTO
                                                 {
                                                     department_id = d.department_id,
                                                     department_name = d.department_name,
                                                     department_short_name = d.department_short_name,
                                                     case_id_int = ho.case_id_int
                                                 }).ToList().ToDictionary(x => x.case_id_int);


                    // select aktuelle Station
                    var departments_current = (from d in st.st_department
                                               join ho in st.st_handled_on
                                               on d.department_id equals ho.department_id
                                               where caseIds.Contains(ho.case_id_int)
                                               && ho.handled_id == (from hh in st.st_handled_on
                                                                       where hh.case_id_int == ho.case_id_int
                                                                       select hh.handled_id).Max()
                                               select new Department_DTO
                                               {
                                                   department_id = d.department_id,
                                                   department_name = d.department_name,
                                                   department_short_name = d.department_short_name,
                                                   case_id_int = ho.case_id_int
                                               }).ToList().ToDictionary(x => x.case_id_int);



                    // select Projekte eines Falls
                    var projects = (from p in st.st_project
                                    select new Project_DTO
                                    {
                                        project_id_int = p.project_id_int,
                                        project_id_ext = p.project_id_ext,
                                        project_name = p.project_name,
                                        project_admin = p.project_admin,
                                        token = p.token
                                    }).ToList().ToDictionary(x => x.project_id_int); ;

                    var inProjects = (from ip in st.st_in_project
                                      where caseIds.Contains(ip.case_id_int)
                                      //select new { case_id_int = ip.case_id_int, project_id_int = ip.project_id_int }).ToList();
                                      select ip).ToList();




                    // Case-Objekte ergänzen
                    foreach (Hospital_Case_DTO c in cases)
                    {
                        try
                        {
                            try { c.case_start_str = c.case_start.Value.ToString("dd.MM.yyyy"); } catch { }
                            try { c.case_last_message_str = c.case_last_message.Value.ToString("dd.MM.yyyy"); } catch { }

                            c.patient = patients[c.patient_id_int.Value];
                            if (c.patient.date_of_birth != null)
                            {
                                c.patient.date_of_birth_str = c.patient.date_of_birth.Value.ToString("dd.MM.yyyy");
                                c.patient.alter = DateTime.Now.Year - c.patient.date_of_birth.Value.Year;
                                if (c.patient.date_of_birth.Value.Month > DateTime.Now.Month || DateTime.Now.Month == c.patient.date_of_birth.Value.Month && DateTime.Now.Day < c.patient.date_of_birth.Value.Day)
                                {
                                    c.patient.alter--;
                                }
                            }

                            c.ward_admission = wards_admission[c.case_id_int];
                            c.ward_current = wards_current[c.case_id_int];
                            c.department_admission = departments_admission[c.case_id_int];
                            c.department_current = departments_current[c.case_id_int];

                            List<Project_DTO> proli = new List<Project_DTO>();
                            List<st_in_project> projs = inProjects.FindAll(x => x.case_id_int == c.case_id_int);
                            c.projects_str = "";
                            foreach (var p in projs)
                            {
                                proli.Add(projects[p.project_id_int]);
                                c.projects_str += projects[p.project_id_int].project_name + ";";
                            }

                            c.projects = proli.ToArray();
                        }
                        catch
                        {
                            continue;
                        }
                    }


                    return cases;



                    //// noch eingrenzen auf Stationen und Benutzer...!!!
                    //var selectResult = (from s in st.st_hospital_case
                    //                   select s).ToList();

                    //foreach (st_hospital_case hcase in selectResult)
                    //{
                    //    var inProjects = (from p in st.st_in_project
                    //                   where p.case_id_int == hcase.case_id_int
                    //                   select p).ToList();

                    //    hcase.st_in_project = inProjects;

                    //    st_project[] projects = new st_project[hcase.st_in_project.Count];
                    //    int i = 0;

                    //    foreach (st_in_project inproject in hcase.st_in_project)
                    //    {
                    //        var project = (from p in st.st_project
                    //                       where p.project_id_int == inproject.project_id_int
                    //                       select p).FirstOrDefault();

                    //        projects[i] = project;
                    //        i++;
                    //    }

                    //    hcase.projects = projects;

                    //    hcase.ward = (from w in st.st_ward
                    //                  where w.ward_id_int == (from h in st.st_handled_on
                    //                                      where h.case_id_int == hcase.case_id_int && h.transfer_date == (from hh in st.st_handled_on
                    //                                                                                                      where hh.case_id_int == hcase.case_id_int
                    //                                                                                                      select hh.transfer_date).Max()
                    //                                      select h.ward_id_int).FirstOrDefault()
                    //                  select w).FirstOrDefault();



                    //    hcase.st_patient = (from p in st.st_patient
                    //                        where p.patient_id_int == hcase.patient_id_int
                    //                        select p).FirstOrDefault();


                    //}


                    //List<Hospital_Case_DTO> dtoCases = new List<Hospital_Case_DTO>();

                    //foreach (st_hospital_case hcase in selectResult)
                    //{
                    //    Hospital_Case_DTO dtoCase = new Hospital_Case_DTO();
                    //    //dtoCase.case_description = hcase.case_description;
                    //    dtoCase.case_end = hcase.case_end;
                    //    dtoCase.case_id_ext = hcase.case_id_ext;
                    //    dtoCase.case_id_int = hcase.case_id_int;
                    //    dtoCase.case_start = hcase.case_start.Value;
                    //    dtoCase.case_start_str = hcase.case_start.Value.ToString("dd.MM.yyyy");
                    //    dtoCase.patient = new Patient_DTO();
                    //    dtoCase.patient.date_of_birth = hcase.st_patient.date_of_birth.Value;
                    //    dtoCase.patient.date_of_birth_str = hcase.st_patient.date_of_birth.Value.ToString("dd.MM.yyyy");
                    //    dtoCase.patient.first_name = hcase.st_patient.first_name;
                    //    dtoCase.patient.last_name = hcase.st_patient.last_name;
                    //    dtoCase.patient.patient_id_ext = hcase.st_patient.patient_id_ext;
                    //    dtoCase.patient.patient_id_int = hcase.st_patient.patient_id_int;
                    //    if(hcase.ward != null)
                    //    {
                    //        dtoCase.ward = new Ward_DTO();
                    //        dtoCase.ward.ward_id_int = hcase.ward.ward_id_int;
                    //        dtoCase.ward.ward_id_ext = hcase.ward.ward_id_ext;
                    //        //dtoCase.ward.ward_manager = hcase.ward.ward_manager;
                    //        dtoCase.ward.ward_name = hcase.ward.ward_name;
                    //    }

                    //    if(hcase.projects != null)
                    //    {
                    //        dtoCase.projects = new Project_DTO[hcase.projects.Length];

                    //        int y = 0;
                    //        foreach (st_project hproject in hcase.projects)
                    //        {
                    //            Project_DTO project = new Project_DTO();
                    //            project.project_admin = hproject.project_admin;
                    //            project.project_id_ext = hproject.project_id_ext;
                    //            project.project_id_int = hproject.project_id_int;
                    //            project.project_name = hproject.project_name;

                    //            dtoCase.projects[y] = project;
                    //            y++;
                    //        }
                    //    }

                    //    dtoCases.Add(dtoCase);
                    //}

                    //return dtoCases;
                }
            }
            catch (Exception e)
            {

                throw e;
            }

        }
    }

}