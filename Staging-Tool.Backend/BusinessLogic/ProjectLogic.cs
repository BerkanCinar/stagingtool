﻿using Newtonsoft.Json;
using ServiceStack;
using Staging_Tool.Backend.DataAccess;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;

namespace Staging_Tool.Backend.BusinessLogic
{
    public class ProjectLogic
    {

        public ProjectLogic()
        {
        }


        public List<st_project> getProjects(UserRightIDs permissions)
        {
            try
            {
                using (var st = new StagingToolModel())
                {
                    if (permissions.admin)
                        return st.st_project.ToList();
                    else
                    {
                        return st.st_project.Where(w => permissions.projectIds.Contains(w.project_id_int)).ToList();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

        }

        public bool deleteProject(long ProjectID)
        {
            try
            {
                using (var st = new StagingToolModel())
                {
                    var projectToDelete = st.st_project.FirstOrDefault(f => f.project_id_int == ProjectID);

                    if (projectToDelete == null) return false;

                    st.st_project.Remove(projectToDelete);
                    st.SaveChanges();

                    return true;
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public bool modifyProject(st_project project)
        {

            try
            {

                string url = ConfigurationManager.AppSettings["redcapurl"];

                if (url == null) return false;

                string paramQuery = "token=" + project.token + "&content=project&format=json";
                var tmp = url.PostToUrl(paramQuery);


                var redCapProject = JsonConvert.DeserializeObject<RedCapProject>(tmp, new JsonSerializerSettings
                {
                    NullValueHandling = NullValueHandling.Ignore
                });
                project.project_name = redCapProject.project_title;
                project.project_id_ext = Convert.ToInt32(redCapProject.project_id);


                using (var st = new StagingToolModel())
                {
                    var modifyProject = st.st_project.FirstOrDefault(f => f.project_id_int == project.project_id_int || f.token == project.token);
                    var newProject = false;
                    if (modifyProject == null)
                    {
                        modifyProject = new st_project();
                        newProject = true;
                    }

                    modifyProject.token = project.token;
                    modifyProject.project_name = project.project_name;
                    modifyProject.project_id_ext = project.project_id_ext;
                    modifyProject.project_admin = "Benutzer";

                    if (newProject)
                    {
                        st.st_project.Add(modifyProject);
                    }

                    st.SaveChanges();
                    return true;
                }
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
                return false;
            }
        }

        public class RedCapProject
        {


            public string Token { get; set; }
            public string project_id { get; set; }
            public string project_title { get; set; }
            public string creation_time { get; set; }
            public string production_time { get; set; }
            public string in_production { get; set; }
            public string project_language { get; set; }
            public string purpose { get; set; }
            public string purpose_other { get; set; }
            public string project_notes { get; set; }
            public string custom_record_label { get; set; }
            public string secondary_unique_field { get; set; }
            public string is_longitudinal { get; set; }
            public string surveys_enabled { get; set; }
            public string scheduling_enabled { get; set; }
            public string record_autonumbering_enabled { get; set; }
            public string randomization_enabled { get; set; }
            public string ddp_enabled { get; set; }
            public string project_irb_number { get; set; }
            public string project_grant_number { get; set; }
            public string project_pi_firstname { get; set; }
            public string project_pi_lastname { get; set; }
            public string display_today_now_button { get; set; }

        }
    }
}
