﻿using Staging_Tool.Backend.DataAccess;
using Staging_Tool.Backend.DataAcessDTO;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Staging_Tool.Backend.BusinessLogic
{
    public class PatientLogic
    {
        private readonly int userId;

        public PatientLogic(int id)
        {
            userId = id;
        }

        // Bestimmung der Patienten festlegen
        public List<PatientInProject> patientInProject(List<int> projectIDs, List<int> caseIDs)
        {
            try
            {
                using (var st = new StagingToolModel())
                {
                    var ret = new List<PatientInProject>();
                    // referenceId als tracker für die Schleife --> referenceId verknüpft zusammenhängende Datensätze - sprich gleicher Patient mit unterschiedlichen Fällen ins selbe Projekt
                    int referenceId = 0;

                    foreach (var projectID in projectIDs)
                    {
                        var projects = st.st_in_project.Where(w => w.project_id_int == projectID).ToList();

                        foreach (var caseID in caseIDs)
                        {
                            referenceId++;
                            var tmp = new PatientInProject();
                            tmp.reference_id = referenceId;

                            tmp.ignore = false;
                            tmp.instance = false;
                            tmp.newRecord = false;

                            var caseInProject = projects.FirstOrDefault(w => w.case_id_int == caseID);
                            var hospital_case = st.st_hospital_case.FirstOrDefault(w => w.case_id_int == caseID);

                            // Projektobj laden
                            tmp.project = st.st_project.Where(w => w.project_id_int == projectID).Select(s => new Project_DTO()
                            {
                                project_name = s.project_name,
                                project_admin = s.project_admin,
                                project_id_ext = s.project_id_ext,
                                project_id_int = s.project_id_int
                            }).SingleOrDefault();

                            // Caseobj laden
                            tmp.hospital_case = new Hospital_Case_DTO()
                            {
                                case_end = hospital_case.case_end,
                                case_id_ext = hospital_case.case_id_ext,
                                case_id_int = hospital_case.case_id_int,
                                case_start = hospital_case.case_start.Value,
                                admission_diagnosis = hospital_case.admission_diagnosis,
                                admission_reason = hospital_case.admission_reason,
                                case_kind = hospital_case.case_kind,
                                discharge_diagnosis = hospital_case.discharge_diagnosis,
                                discharge_reason = hospital_case.discharge_reason
                            };

                            tmp.patient = st.st_patient.Where(w => w.patient_id_int == hospital_case.patient_id_int).Select(s => new Patient_DTO()
                            {
                                patient_id_int = s.patient_id_int,
                                date_of_birth = s.date_of_birth.Value,
                                first_name = s.first_name,
                                last_name = s.last_name,
                                patient_id_ext = s.patient_id_ext,
                                city = s.city,
                                gender = s.gender,
                                street = s.street,
                                zip_code = s.zip_code

                            }).SingleOrDefault();


                            if (caseInProject == null)
                            {
                                // Case noch nicht vorhanden!
                                // prüfen ob patient in liste
                                var patientWithSameProjectAndDiffCase = ret.Where(w => w.patient.patient_id_int == tmp.patient.patient_id_int && w.project.project_id_int == tmp.project.project_id_int && !w.ignore).ToList();


                                if (patientWithSameProjectAndDiffCase.Count > 0)
                                {


                                    if (patientWithSameProjectAndDiffCase.First().newRecord && !patientWithSameProjectAndDiffCase.First().instance)
                                        tmp.listInstance = true;

                                    if (patientWithSameProjectAndDiffCase.First().instance)
                                        tmp.newRecord = patientWithSameProjectAndDiffCase.First().newRecord;
                                    tmp.instance = patientWithSameProjectAndDiffCase.First().instance;
                                    tmp.record_id = patientWithSameProjectAndDiffCase.First().record_id;
                                    tmp.reference_id = patientWithSameProjectAndDiffCase.First().reference_id;

                                    ret.Add(tmp);
                                    continue;
                                }


                                // Prüfen ob der Patient bereits einen anderen case im Projekt hat ! 

                                if (hospital_case != null)
                                {
                                    var casesInProject = st.st_hospital_case.Where(w => w.patient_id_int == hospital_case.patient_id_int).ToList();

                                    foreach (var patientCaseInProject in casesInProject)
                                    {
                                        var patientInProject = projects.Where(w => w.case_id_int == patientCaseInProject.case_id_int).ToList();

                                        if (patientInProject.Count > 0)
                                        {
                                            // Patient bereits im Projekt. neue Instanz anbieten
                                            tmp.instance = true;
                                            tmp.newRecord = true;
                                            tmp.record_id = patientInProject.First().record_id;

                                            break;
                                        }

                                    }
                                    tmp.newRecord = true;

                                }
                                else
                                {
                                    throw new Exception("ID muss vorhanden sein, da der Nutzer diesen vorher selektiert hat");
                                }


                            }
                            else
                            {
                                // der case ist bereits im Projekt!!
                                tmp.record_id = caseInProject.record_id;
                                tmp.ignore = true;

                            }
                            ret.Add(tmp);


                        }
                    }

                    return ret.OrderBy(o => o.reference_id).ToList();

                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }



        // temporary Backup Code


        //public List<PatientInProject> patientInProject(List<int> projectIDs, List<int> caseIDs)
        //{
        //    try
        //    {
        //        using (var st = new StagingToolModel())
        //        {
        //            var ret = new List<PatientInProject>();
        //            // referenceId als tracker für die Schleife --> referenceId verknüpft zusammenhängende Datensätze - sprich gleicher Patient mit unterschiedlichen Fällen ins selbe Projekt
        //            int referenceId = 0;

        //            foreach (var projectID in projectIDs)
        //            {
        //                var projects = st.st_in_project.Where(w => w.project_id_int == projectID).ToList();

        //                foreach (var caseID in caseIDs)
        //                {
        //                    referenceId++;
        //                    var tmp = new PatientInProject();
        //                    tmp.reference_id = referenceId;
        //                    tmp.patientInProject = false;
        //                    tmp.caseInProject = false;

        //                    var caseInProject = projects.FirstOrDefault(w => w.case_id_int == caseID);

        //                    var hospital_case = st.st_hospital_case.FirstOrDefault(w => w.case_id_int == caseID);


        //                    tmp.project = st.st_project.Where(w => w.project_id_int == projectID).Select(s => new Project_DTO()
        //                    {
        //                        project_name = s.project_name,
        //                        project_admin = s.project_admin,
        //                        project_id_ext = s.project_id_ext,
        //                        project_id_int = s.project_id_int
        //                    }).SingleOrDefault();

        //                    tmp.hospital_case = new Hospital_Case_DTO()
        //                    {
        //                        //case_description = hospital_case.case_description,
        //                        case_end = hospital_case.case_end,
        //                        case_id_ext = hospital_case.case_id_ext,
        //                        case_id_int = hospital_case.case_id_int,
        //                        case_start = hospital_case.case_start.Value,
        //                        admission_diagnosis = hospital_case.admission_diagnosis,
        //                        admission_reason = hospital_case.admission_reason,
        //                        case_kind = hospital_case.case_kind,
        //                        discharge_diagnosis = hospital_case.discharge_diagnosis,
        //                        discharge_reason = hospital_case.discharge_reason

        //                    };

        //                    tmp.patient = st.st_patient.Where(w => w.patient_id_int == hospital_case.patient_id_int).Select(s => new Patient_DTO()
        //                    {
        //                        patient_id_int = s.patient_id_int,
        //                        date_of_birth = s.date_of_birth.Value,
        //                        first_name = s.first_name,
        //                        last_name = s.last_name,
        //                        patient_id_ext = s.patient_id_ext,
        //                        city = s.city,
        //                        gender = s.gender,
        //                        street = s.street,
        //                        zip_code = s.zip_code

        //                    }).SingleOrDefault();

        //                    var patientWithSameProjectAndDiffCase = ret.FirstOrDefault(w => w.patient.patient_id_int == tmp.patient.patient_id_int && w.project.project_id_int == tmp.project.project_id_int);

        //                    if (patientWithSameProjectAndDiffCase != null)
        //                    {
        //                        // Vorsicht! Patient zwar im Projekt aber nur weil der Patient zuvor in der Liste vorhanden ist
        //                        tmp.patientInProject = patientWithSameProjectAndDiffCase.patientInProject;
        //                        tmp.record_id = patientWithSameProjectAndDiffCase.record_id;
        //                        tmp.reference_id = patientWithSameProjectAndDiffCase.reference_id;

        //                        ret.Add(tmp);
        //                        continue;
        //                    }
        //                    else
        //                    {
        //                        if (caseInProject == null)
        //                        {
        //                            // Case noch nicht vorhanden!

        //                            // Prüfen ob der Patient bereits einen anderen case im Projekt hat ! 

        //                            if (hospital_case != null)
        //                            {
        //                                var casesInProject = st.st_hospital_case.Where(w => w.patient_id_int == hospital_case.patient_id_int).ToList();

        //                                foreach (var patientCaseInProject in casesInProject)
        //                                {
        //                                    var patientInProject = projects.Where(w => w.case_id_int == patientCaseInProject.case_id_int).ToList();

        //                                    if (patientInProject.Count > 0)
        //                                    {
        //                                        // Patient bereits im Projekt. neue Instanz anbieten
        //                                        tmp.patientInProject = true;
        //                                        tmp.record_id = patientInProject.First().record_id;

        //                                        break;
        //                                    }

        //                                }

        //                            }
        //                            else
        //                            {
        //                                throw new Exception("ID muss vorhanden sein, da der Nutzer diesen vorher selektiert hat");
        //                            }


        //                        }
        //                        else
        //                        {
        //                            // der case ist bereits im Projekt!!
        //                            tmp.record_id = caseInProject.record_id;
        //                            tmp.patientInProject = true;
        //                            tmp.caseInProject = true;

        //                        }
        //                        ret.Add(tmp);
        //                    }

        //                }
        //            }

        //            return ret.OrderBy(o => o.reference_id).ToList();

        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        throw e;
        //    }
        //}
    }
}