﻿using Staging_Tool.Backend.DataAccess;
using Staging_Tool.Backend.DataAcessDTO;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;

namespace Staging_Tool.Backend.BusinessLogic
{
    public class UserLogic
    {

        public bool DeleteUser(int userId)
        {
            try
            {
                using (var st = new StagingToolModel())
                {
                    var user = st.st_user.Where(w => w.user_id == userId).FirstOrDefault();

                    if (user != null)
                    {
                        st.st_ward_auth.Where(w => w.user_id == user.user_id).ToList().ForEach(el =>
                        {
                            st.st_ward_auth.Remove(el);
                        });

                        st.st_project_auth.Where(w => w.user_id == user.user_id).ToList().ForEach(el =>
                        {
                            st.st_project_auth.Remove(el);
                        });

                        st.st_department_auth.Where(w => w.user_id == user.user_id).ToList().ForEach(el =>
                        {
                            st.st_department_auth.Remove(el);
                        });

                        st.st_in_project.Where(w => w.user_id == user.user_id).ToList().ForEach(el =>
                        {
                            el.user_id = -1;
                        });

                        st.st_user.Remove(user);

                        st.SaveChanges();
                        return true;
                    }
                    return false;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public bool modifyUser(UserRightObjects user)
        {
            try
            {
                using (var st = new StagingToolModel())
                {

                    var userToModify = st.st_user.Where(w => w.user_id == user.user.user_id).FirstOrDefault();
                    bool newUser = false;

                    if (userToModify == null)
                    {
                        var userAlreadyExists = st.st_user.Where(w => w.user_name == user.user.user_name).FirstOrDefault();

                        if (userAlreadyExists != null) return false;


                        userToModify = new st_user();
                        userToModify.initial_password = true;
                        userToModify.user_password = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(user.user.user_name + ":evkb"));
                        newUser = true;
                    }

                    userToModify.user_name = user.user.user_name;
                    userToModify.user_valid_from = user.user.user_valid_from;
                    userToModify.user_valid_to = user.user.user_valid_to;
                    userToModify.superuser = user.user.superuser;



                    // vorhandene Berechtigungen löschen
                    if (newUser)
                    {
                        st.st_user.Add(userToModify);
                    }
                    else
                    {
                        st.st_ward_auth.Where(w => w.user_id == user.user.user_id).ToList().ForEach(el =>
                        {
                            st.st_ward_auth.Remove(el);
                        });

                        st.st_project_auth.Where(w => w.user_id == user.user.user_id).ToList().ForEach(el =>
                        {
                            st.st_project_auth.Remove(el);
                        });

                        st.st_department_auth.Where(w => w.user_id == user.user.user_id).ToList().ForEach(el =>
                        {
                            st.st_department_auth.Remove(el);
                        });
                    }
                    st.SaveChanges();

                    if (userToModify.superuser == true)
                    {
                        return true;
                    }

                    // Berechtigungen 
                    user.wards.ForEach(el =>
                    {
                        st_ward_auth wardAuth = new st_ward_auth()
                        {
                            ward_id_int = el.ward_id_int,
                            user_id = userToModify.user_id
                        };
                        st.st_ward_auth.Add(wardAuth);
                    });

                    user.departments.ForEach(el =>
                    {
                        st_department_auth deptAuth = new st_department_auth()
                        {
                            department_id = el.department_id,
                            user_id = userToModify.user_id
                        };
                        st.st_department_auth.Add(deptAuth);
                    });

                    user.projects.ForEach(el =>
                    {
                        st_project_auth projectAuth = new st_project_auth()
                        {
                            project_id_int = el.project_id_int,
                            user_id = userToModify.user_id
                        };
                        st.st_project_auth.Add(projectAuth);
                    });
                    st.SaveChanges();
                    return true;
                }

            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Trace.TraceInformation("Property: {0} Error: {1}",
                                                validationError.PropertyName,
                                                validationError.ErrorMessage);
                    }
                }
                throw;
            }
        }


        public bool resetUserPassword(int userId)
        {
            try
            {
                using (var st = new StagingToolModel())
                {

                    var user = st.st_user.Where(w => w.user_id == userId).FirstOrDefault();

                    if (user == null) return false;

                    user.initial_password = true;
                    user.user_password = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(user.user_name + ":evkb"));

                    st.SaveChanges();

                    return true;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool changePassword(st_user user)
        {
            try
            {
                using (var st = new StagingToolModel())
                {
                    var modifyUser = st.st_user.Where(w => w.user_id == user.user_id).FirstOrDefault();

                    if (modifyUser == null) return false;

                    modifyUser.user_password = user.user_password;
                    modifyUser.initial_password = false;

                    st.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {

                throw e;
            }
            return false;
        }

        public st_user GetUserByUserName(string name)
        {
            try
            {
                using (var st = new StagingToolModel())
                {
                    return st.st_user.Where(w => w.user_name == name).FirstOrDefault();
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public List<UserRightObjects> GetUser()
        {
            // GetUserProjects(1);

            try
            {
                using (var st = new StagingToolModel())
                {
                    var ret =
                        // DIRECT WARDS
                        st.st_user.GroupJoin(st.st_ward_auth, a => a.user_id, b => b.user_id,
                    (a, b) => new
                    {

                        a.user_id,
                        a.user_name,
                        a.superuser,
                        a.user_valid_from,
                        a.user_valid_to,
                        wards = b.Join(st.st_ward, x => x.ward_id_int, y => y.ward_id_int, (x, y) => new Ward_DTO()
                        {
                            ward_name = y.ward_name,
                            ward_id_int = y.ward_id_int,
                            ward_id_ext = y.ward_id_ext,
                        })
                    })

                    // PROJECTS
                    .GroupJoin(st.st_project_auth, a => a.user_id, b => b.user_id, (a, b) => new
                    {
                        a.user_id,
                        a.user_name,
                        a.superuser,
                        a.user_valid_from,
                        a.user_valid_to,
                        a.wards,
                        projects = b.Join(st.st_project, x => x.project_id_int, y => y.project_id_int, (x, y) => new Project_DTO()
                        {
                            project_admin = y.project_admin,
                            project_id_ext = y.project_id_ext,
                            project_id_int = y.project_id_int,
                            project_name = y.project_name
                        })
                    })
                    // CASES
                    .GroupJoin(st.st_in_project, a => a.user_id, b => b.user_id, (a, b) => new
                    {
                        a.user_id,
                        a.user_name,
                        a.superuser,
                        a.user_valid_from,
                        a.user_valid_to,
                        a.wards,
                        a.projects,

                        cases = b.Join(st.st_hospital_case, x => x.case_id_int, y => y.case_id_int, (x, y) => new
                        {
                            //y.case_description,
                            y.case_end,
                            y.case_id_ext,
                            y.case_id_int,
                            y.case_start,
                            y.patient_id_int

                        })
                        // GET PATIENT OBJECT
                        .Join(st.st_patient, p => p.patient_id_int, c => c.patient_id_int, (p, c) => new Hospital_Case_DTO()
                        {
                            case_end = p.case_end,
                            case_id_ext = p.case_id_ext,
                            case_id_int = p.case_id_int,
                            case_start = p.case_start.Value,
                            patient = new Patient_DTO()
                            {
                                date_of_birth = c.date_of_birth.Value,
                                patient_id_int = c.patient_id_int,
                                first_name = c.first_name,
                                last_name = c.last_name,
                                patient_id_ext = c.patient_id_ext
                            }
                        }).ToList()
                    })


                    // DEPARTMENT WARDS
                    .GroupJoin(st.st_department_auth, a => a.user_id, b => b.user_id, (a, b) => new
                    {
                        a.user_id,
                        a.user_name,
                        a.superuser,
                        a.user_valid_from,
                        a.user_valid_to,
                        a.wards,
                        a.projects,
                        a.cases,
                        departments = (b.Join(st.st_department, x => x.department_id, y => y.department_id, (x, y) => new
                        {
                            y.department_name,
                            y.department_id,

                        })).GroupJoin(st.st_department_ward, e => e.department_id, f => f.department_id, (e, f) => new Department_DTO
                        {
                            department_id = e.department_id,
                            department_name = e.department_name,

                            wards = f.Join(st.st_ward, g => g.ward_id_int, h => h.ward_id_int, (g, h) => new Ward_DTO()
                            {
                                ward_name = h.ward_name,
                                ward_id_int = h.ward_id_int,
                                ward_id_ext = h.ward_id_ext,
                            }).ToList()
                        })
                    })
                    .Select(s => new UserRightObjects()
                    {
                        user = new User_DTO()
                        {
                            superuser = s.superuser,
                            user_id = s.user_id,
                            user_name = s.user_name,
                            user_valid_from = s.user_valid_from.Value,
                            user_valid_to = s.user_valid_to
                        },
                        cases = s.cases.ToList(),
                        wards = s.wards.ToList(),
                        projects = s.projects.ToList(),
                        departments = s.departments.ToList()
                    })

                    .ToList();




                    return ret;


                }
            }
            catch (Exception e)
            {
                throw e;
            }
        }
    }




}
