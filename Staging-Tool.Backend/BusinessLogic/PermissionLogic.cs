﻿using Staging_Tool.Backend.DataAccess;
using Staging_Tool.Backend.DataAcessDTO;
using System;
using System.Linq;

namespace Staging_Tool.Backend.BusinessLogic
{
    public class PermissionLogic
    {
        public Permission GetPermissionOptions()
        {
            try
            {
                using (var st = new StagingToolModel())
                {
                    Permission ret = new Permission();

                    ret.projects = st.st_project.ToList();


                    ret.departments = st.st_department.GroupJoin(st.st_department_ward, a => a.department_id, b => b.department_id, (a, b) => new Department_DTO
                    {
                        department_id = a.department_id,
                        department_name = a.department_name,
                        department_short_name = a.department_short_name,
                        wards = b.Join(st.st_ward, c => c.ward_id_int, d => d.ward_id_int, (c, d) => new Ward_DTO
                        {
                            ward_id_ext = d.ward_id_ext,
                            ward_id_int = d.ward_id_int,
                            ward_name = d.ward_name,
                            ward_short_name = d.ward_short_name
                        }).ToList()

                    }).ToList();

                    ret.wards = st.st_ward.ToList();

                    return ret;
                }

            }
            catch (Exception e)
            {

                throw e;
            }

        }
    }

}
