﻿using Staging_Tool.Backend.DataAccess;
using Staging_Tool.Backend.DataAcessDTO;
using Staging_Tool.Backend.Resp;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Staging_Tool.Backend.BusinessLogic
{
    public class DepartmentLogic
    {

        public bool DeleteDepartment(int deptId)
        {
            try
            {
                using (var st = new StagingToolModel())
                {
                    var dept = st.st_department.Where(w => w.department_id == deptId).FirstOrDefault();

                    if (dept != null)
                    {
                        st.st_department_auth.Where(w => w.department_id == dept.department_id).ToList().ForEach(el =>
                        {
                            st.st_department_auth.Remove(el);
                        });

                        st.st_department_ward.Where(w => w.department_id == dept.department_id).ToList().ForEach(el =>
                        {
                            st.st_department_ward.Remove(el);
                        });

                        st.st_department.Remove(dept);

                        st.SaveChanges();
                        return true;
                    }
                    return false;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public bool modifyDepartment(Department_DTO dept)
        {
            try
            {
                using (var st = new StagingToolModel())
                {

                    var deptToModify = st.st_department.Where(w => w.department_id == dept.department_id).FirstOrDefault();
                    bool newDept = false;

                    if (deptToModify == null)
                    {
                        deptToModify = new st_department();
                        newDept = true;
                    }
                    deptToModify.department_name = dept.department_name;
                    deptToModify.department_short_name = dept.department_short_name;


                    if (newDept)
                    {
                        st.st_department.Add(deptToModify);
                    }
                    else
                    {
                        st.st_department_auth.Where(w => w.department_id == dept.department_id).ToList().ForEach(el =>
                        {
                            st.st_department_auth.Remove(el);
                        });

                        st.st_department_ward.Where(w => w.department_id == dept.department_id).ToList().ForEach(el =>
                        {
                            st.st_department_ward.Remove(el);
                        });
                    }
                    st.SaveChanges();

                    // Berechtigungen 
                    dept.wards.ForEach(el =>
                    {
                        st_department_ward wardDeptAuth = new st_department_ward()
                        {
                            ward_id_int = el.ward_id_int,
                            department_id = deptToModify.department_id
                        };
                        st.st_department_ward.Add(wardDeptAuth);
                    });


                    st.SaveChanges();
                    return true;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public DepartmentResponse getDepartmentsAndWards()
        {
            try
            {
                using (var st = new StagingToolModel())
                {
                    DepartmentResponse ret = new DepartmentResponse();

                    ret.departments = st.st_department.GroupJoin(st.st_department_ward, a => a.department_id, b => b.department_id, (a, b) => new Department_DTO()
                    {
                        department_id = a.department_id,
                        department_name = a.department_name,
                        department_short_name = a.department_short_name,
                        wards = b.Join(st.st_ward, c => c.ward_id_int, d => d.ward_id_int, (c, d) => new Ward_DTO
                        {
                            ward_id_int = c.ward_id_int,
                            ward_id_ext = d.ward_id_ext,
                            ward_name = d.ward_name,
                            ward_short_name = d.ward_short_name

                        }).ToList()
                    }).ToList();


                    return ret;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public bool resetUserPassword(int userId)
        {
            try
            {
                using (var st = new StagingToolModel())
                {

                    var user = st.st_user.Where(w => w.user_id == userId).FirstOrDefault();

                    if (user == null) return false;

                    user.initial_password = true;
                    user.user_password = "evkb";

                    st.SaveChanges();

                    return true;
                }

            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public bool changePassword(st_user user)
        {
            try
            {
                using (var st = new StagingToolModel())
                {
                    var modifyUser = st.st_user.Where(w => w.user_id == user.user_id).FirstOrDefault();

                    if (modifyUser == null) return false;

                    modifyUser.user_password = user.user_password;
                    modifyUser.initial_password = false;

                    st.SaveChanges();
                    return true;
                }
            }
            catch (Exception e)
            {

                throw e;
            }
            return false;
        }

        public st_user GetUserByUserName(string name)
        {
            try
            {
                using (var st = new StagingToolModel())
                {
                    return st.st_user.Where(w => w.user_name == name).FirstOrDefault();
                }
            }
            catch (Exception e)
            {

                throw e;
            }
        }

        public List<UserRightObjects> GetUser()
        {

            try
            {
                using (var st = new StagingToolModel())
                {
                    // direkt unterstelle wards
                    var ret =
                        st.st_user.GroupJoin(st.st_ward_auth, a => a.user_id, b => b.user_id,
                    (a, b) => new
                    {

                        a.user_id,
                        a.user_name,
                        a.superuser,
                        a.user_valid_from,
                        a.user_valid_to,
                        wards = b.Join(st.st_ward, x => x.ward_id_int, y => y.ward_id_int, (x, y) => new Ward_DTO()
                        {
                            ward_name = y.ward_name,
                            ward_id_int = y.ward_id_int,
                            ward_id_ext = y.ward_id_ext,
                        })
                    })

                    // PROJECTS
                    .GroupJoin(st.st_project_auth, a => a.user_id, b => b.user_id, (a, b) => new
                    {
                        a.user_id,
                        a.user_name,
                        a.superuser,
                        a.user_valid_from,
                        a.user_valid_to,
                        a.wards,
                        projects = b.Join(st.st_project, x => x.project_id_int, y => y.project_id_int, (x, y) => new Project_DTO()
                        {
                            project_admin = y.project_admin,
                            project_id_ext = y.project_id_ext,
                            project_id_int = y.project_id_int,
                            project_name = y.project_name
                        })
                    })
                    // CASES
                    .GroupJoin(st.st_in_project, a => a.user_id, b => b.user_id, (a, b) => new
                    {
                        a.user_id,
                        a.user_name,
                        a.superuser,
                        a.user_valid_from,
                        a.user_valid_to,
                        a.wards,
                        a.projects,

                        cases = b.Join(st.st_hospital_case, x => x.case_id_int, y => y.case_id_int, (x, y) => new
                        {
                            y.case_end,
                            y.case_id_ext,
                            y.case_id_int,
                            y.case_start,
                            y.patient_id_int

                        })
                        // GET PATIENT OBJECT
                        .Join(st.st_patient, p => p.patient_id_int, c => c.patient_id_int, (p, c) => new Hospital_Case_DTO()
                        {
                            case_end = p.case_end,
                            case_id_ext = p.case_id_ext,
                            case_id_int = p.case_id_int,
                            case_start = p.case_start.Value,
                            patient = new Patient_DTO()
                            {
                                date_of_birth = c.date_of_birth.Value,
                                patient_id_int = c.patient_id_int,
                                first_name = c.first_name,
                                last_name = c.last_name,
                                patient_id_ext = c.patient_id_ext
                            }
                        }).ToList()
                    })


                    // DEPARTMENT WARDS
                    .GroupJoin(st.st_department_auth, a => a.user_id, b => b.user_id, (a, b) => new
                    {
                        a.user_id,
                        a.user_name,
                        a.superuser,
                        a.user_valid_from,
                        a.user_valid_to,
                        a.wards,
                        a.projects,
                        a.cases,
                        departments = (b.Join(st.st_department, x => x.department_id, y => y.department_id, (x, y) => new
                        {
                            y.department_name,
                            y.department_id,

                        })).GroupJoin(st.st_department_ward, e => e.department_id, f => f.department_id, (e, f) => new Department_DTO
                        {
                            department_id = e.department_id,
                            department_name = e.department_name,

                            wards = f.Join(st.st_ward, g => g.ward_id_int, h => h.ward_id_int, (g, h) => new Ward_DTO()
                            {
                                ward_name = h.ward_name,
                                ward_id_int = h.ward_id_int,
                                ward_id_ext = h.ward_id_ext,
                            }).ToList()
                        })
                    })
                    .Select(s => new UserRightObjects()
                    {
                        user = new User_DTO()
                        {
                            superuser = s.superuser,
                            user_id = s.user_id,
                            user_name = s.user_name,
                            user_valid_from = s.user_valid_from.Value,
                            user_valid_to = s.user_valid_to
                        },
                        cases = s.cases.ToList(),
                        wards = s.wards.ToList(),
                        projects = s.projects.ToList(),
                        departments = s.departments.ToList()
                    })

                    .ToList();


                    return ret;


                }
            }
            catch (Exception e)
            {
                // bei convert Problem --> convert zero datetime=True; in den connection string in der web.config
                throw e;
            }
        }
    }




}
