// Angular
import '@angular/platform-browser';
import '@angular/platform-browser-dynamic';
import '@angular/core';
import '@angular/common';
import '@angular/http';
import '@angular/router';
import '@angular/animations';
import '@angular/platform-browser/animations';

// RxJS
import 'rxjs';
// Other vendors for example jQuery, Lodash or Bootstrap
// You can import js, ts, css, sass, ...


import './node_modules/font-awesome/css/font-awesome.min.css';
import './public/css/bootstrap.min.css';
import './public/css/styles.css'; 
import './node_modules/primeng/resources/themes/omega/theme.css'
import './node_modules/primeng/resources/primeng.min.css';


import 'angular-2-dropdown-multiselect';
import 'primeng/primeng';

//import '@swimlane/ngx-datatable';
//import '@covalent/core';



   