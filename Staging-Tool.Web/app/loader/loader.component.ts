import { Component, OnInit } from '@angular/core';
import { LoaderService } from './loader.service';

@Component({
    moduleId: module.id.toString(),
    selector: 'app-loader',
    templateUrl: './loader.component.html',
    styleUrls: ['./loader.component.css']
})
export class LoaderComponent implements OnInit {
    constructor(private loaderService: LoaderService) { }

    active: boolean = false;

    ngOnInit(): void {
        this.loaderService.loaderStatus$.subscribe(loaderStatus => {
            var tempStatus = { loaderStatus };
            this.active = <boolean>tempStatus.loaderStatus;
        })
    }
}