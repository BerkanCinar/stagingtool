import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class LoaderService {
    loaderSource = new Subject();
    loaderStatus$ = this.loaderSource.asObservable();
}