﻿// Global interfaces 
export interface IProject {
    project_id_int: number;
    project_id_ext: string;
    project_name: string;
    project_admin: string;
}

export interface IPatient {
    patient_id_int: number;
    patient_id_ext: number;
    last_name: string;
    first_name: string;
    date_of_birth: Date;
    admission: Date;
}


// Internal Angular classes (only internal Use)
export class Project implements IProject {
    project_id_int: number;
    project_id_ext: string;
    project_name: string;
    project_admin: string;
    selected: boolean = false;
    token: string

    constructor() {

        this.token = null;
        this.project_id_ext = null;
        this.project_id_int = null;
        this.project_name = null;
        this.project_admin = null;
    }
}



export class Notification {
    Message: string;
    Type: NotificationType;

    constructor(msg: string, noteType: NotificationType) {
        this.Message = msg;
        this.Type = noteType;
    }
}

export class ModalClose {
    Type: ModalCancelReason;
    Id: number;
    Obj: any;
}

// Internal Angular ENUMS

// ENUMS
export enum ModalCancelReason {
    CANCEL,
    DELETE,
    RESET,
    SUBMIT
}

export enum NotificationType {
    ERROR,
    SUCCESS,
    WARN
}

export enum AdminState {
    USERS,
    DEPARTMENTS
}

export enum DetailState {
    DEPARTMENTS,
    WARDS,
    CASES,
    PROJECTS
}

export enum PatientDestination {
    IGNORE = <any>'IGNORE',
    NEW = <any>'NEW',
    INSTANCE = <any>'INSTANCE'
}


// Backend Classes

export class UserRight {
    user: User;
    projects: Project[];
    wards: Ward[];
    cases: Case[];
    departments: Department[];

    expandDepartment: boolean = false;
    expandProject: boolean = false;
    expandCase: boolean = false;
    expandWard: boolean = false;

    constructor() {
        this.user = new User();
        this.projects = [];
        this.wards = [];
        this.cases = [];
        this.departments = [];
    }
}

export class Patient implements IPatient {
    patient_id_int: number;
    patient_id_ext: number;
    last_name: string;
    first_name: string;
    date_of_birth: Date;
    gender: string;
    street: string;
    zip_code: string;
    city: string;
    admission: Date;
    alter: number;
    date_of_birth_str: string;

    constructor() {
        this.patient_id_ext = null;
        this.patient_id_int = null;
        this.first_name = null;
        this.last_name = null;
        this.date_of_birth = null;
        this.gender = null;
        this.street = null;
        this.zip_code = null;
        this.city = null;
        this.admission = null;
        this.alter = null;

        this.date_of_birth_str = null;
    }
}

export class Permission {
    wards: Ward[];
    departments: Department[];
    projects: Project[];

    constructor() {
        this.wards = [new Ward()];
        this.departments = [new Department()];
        this.projects = [new Project()];
    }
}

export class User {
    user_name: string;
    user_id: number;
    user_valid_from: Date;
    user_valid_to: Date;
    superuser: boolean;
    user_password: string;
    initial_password: boolean;

    constructor() {
        this.user_id = null;
        this.user_name = null;
        this.superuser = false;
        this.user_valid_from = null;
        this.user_valid_to = null;
    }
}

export class Case {
    case_id_int: number;
    case_id_ext: number;
    case_kind: string;
    admission_reason: string;
    discharge_reason: string;
    referral_doctor: string;
    referral_hospital: string;
    admission_diagnosis: string;
    discharge_diagnosis: string;
    case_start: Date;
    case_end: Date;
    patient_id_int: number;
    case_last_message: Date;

    patient: Patient;
    ward_admission: Ward;
    ward_current: Ward;
    department_admission: Department;
    department_current: Department;
    projects: Project[];
    selected: boolean = false;

    projects_str: string;
    case_start_str: string;
    case_last_message_str: string;

    // nicht sicher ob das so implementiert wird
    //caseDestination: CaseDestination;

    constructor() {
        this.case_id_int = null;
        this.case_id_ext = null;
        this.case_kind = null;
        this.admission_reason = null;
        this.discharge_reason = null;
        this.referral_doctor = null;
        this.referral_hospital = null;
        this.admission_diagnosis = null;
        this.discharge_diagnosis = null;
        this.case_start = null;
        this.case_end = null;
        this.patient_id_int = null;
        this.case_last_message = null;

        this.patient = new Patient();
        this.ward_admission = new Ward();
        this.ward_current = new Ward();
        this.projects = [new Project()];
        this.selected = false;
    }
}

//export class CaseDestination {
//    destination: PatientDestination;
//    project: Project;
//    patient: Patient;
//    hospital_case: Case;
//    caseInProject: boolean;
//    patientInProject: boolean;
//}

export class DepartmentData {
    departments: Department[];
    wards: Ward[];

    constructor() {
        this.wards = [];
        this.departments = [];
    }
}

export class Department {
    department_id: number;
    department_name: string;
    department_short_name: string;

    wards: Ward[];

    constructor() {
        this.department_id = null;
        this.department_name = null;
        this.department_short_name = null;
        this.wards = [];
    }
}

export class Ward {
    ward_id_int: number;
    ward_id_ext: string;
    ward_name: string;
    ward_short_name: string;

    constructor() {
        this.ward_id_int = null;
        this.ward_id_ext = null;
        this.ward_name = null;
        this.ward_short_name = null;
    }
}

export class PatientInProject {
    project: Project;
    patient: Patient;
    hospital_case: Case;
    caseInProject: boolean;
    patientInProject: boolean;
    record_id: string;

    // +++++
    newRecord: boolean;
    ignore: boolean;
    instance: boolean;
    listInstance: boolean;
    // +++++

    reference_id: number;

    // Frontend values
    enableNewPatient: boolean;
    enableInstance: boolean;
    destination: PatientDestination;

}


// RedCap Classes
export class RedCapProject {
    Token: string;
    project_id: string;
    project_title: string;
    creation_time: string;
    production_time: string;
    in_production: string;
    project_language: string;
    purpose: string;
    purpose_other: string;
    project_notes: string;
    custom_record_label: string;
    secondary_unique_field: string;
    is_longitudinal: string;
    surveys_enabled: string;
    scheduling_enabled: string;
    record_autonumbering_enabled: string;
    randomization_enabled: string;
    ddp_enabled: string;
    project_irb_number: string;
    project_grant_number: string;
    project_pi_firstname: string;
    project_pi_lastname: string;
    display_today_now_button: string;
}

