﻿import { Injectable } from '@angular/core';
import { Headers, Http, ResponseContentType } from '@angular/http';
import { LoaderService } from '../loader/loader.service';
import 'rxjs/add/operator/toPromise';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';



@Injectable()
export class HttpClientService {

    httpSource = new Subject();
    httpAuthentication$ = this.httpSource.asObservable();


    constructor(private loaderService: LoaderService, private http: Http) { }

    createCustomHeader(headers: Headers) {
        headers.append('InstanceCurrentLanguage', 'de-DE');
        headers.append('Accept', 'application/json, text/plain, */*');

    }



    handleError(errorObject: any): string {
        this.loaderService.loaderSource.next(false);

        if (errorObject.status === 500) {
            let responseContent = JSON.parse(errorObject['_body']);

            if (responseContent != undefined && responseContent.StatusCode != undefined && responseContent.StatusCode === 401) {
                this.httpSource.next(false);
            }

            if (responseContent != undefined && responseContent.Description != undefined) {
                return responseContent.Description;
            }

            return responseContent;
        }

        if (errorObject.status === 401) {
            return errorObject;
        }

        return errorObject.statusText;
    };



    get(url: string): Promise<Response> {
        this.loaderService.loaderSource.next(true);

        let headers = new Headers();
        this.createCustomHeader(headers);

        return this.http.get(url, { headers: headers, withCredentials: true }).toPromise()
            .then(response => {
                this.loaderService.loaderSource.next(false);

                return new Promise<any>((resolve, reject) => {
                    resolve(response);
                });
            })
            .catch(error => {
                var response = this.handleError(error);

                return new Promise<any>((resolve, reject) => {
                    reject(response);
                });
            });
    }

    post(url: string, data: any): Promise<Response> {
        this.loaderService.loaderSource.next(true);

        let headers = new Headers();
        this.createCustomHeader(headers);

        return this.http.post(url, data, {
            headers: headers, withCredentials: true
        }).toPromise()
            .then(response => {
                this.loaderService.loaderSource.next(false);

                return new Promise<any>((resolve, reject) => {
                    resolve(response);
                });
            })
            .catch(error => {
                var response = this.handleError(error);

                return new Promise<any>((resolve, reject) => {
                    reject(response);
                });
            });
    }

    putWithId(url: string, id: number): Promise<Response> {
        this.loaderService.loaderSource.next(true);

        let headers = new Headers();
        this.createCustomHeader(headers);

        return this.http.put(url + "/" + id, {
            headers: headers, withCredentials: true
        }).toPromise()
            .then(response => {
                this.loaderService.loaderSource.next(false);

                return new Promise<any>((resolve, reject) => {
                    resolve(response);
                });
            })
            .catch(error => {
                var response = this.handleError(error);

                return new Promise<any>((resolve, reject) => {
                    reject(response);
                });
            });
    }

    put(url: string, data: any): Promise<Response> {
        this.loaderService.loaderSource.next(true);

        let headers = new Headers();
        this.createCustomHeader(headers);

        return this.http.put(url, data, {
            headers: headers, withCredentials: true
        }).toPromise()
            .then(response => {
                this.loaderService.loaderSource.next(false);

                return new Promise<any>((resolve, reject) => {
                    resolve(response);
                });
            })
            .catch(error => {
                var response = this.handleError(error);

                return new Promise<any>((resolve, reject) => {
                    reject(response);
                });
            });
    }

    delete(url: string, id: number): Promise<Response> {
        this.loaderService.loaderSource.next(true);

        let headers = new Headers();
        this.createCustomHeader(headers);

        return this.http.delete(url + "/" + id, {
            headers: headers, withCredentials: true
        }).toPromise()
            .then(response => {
                this.loaderService.loaderSource.next(false);

                return new Promise<any>((resolve, reject) => {
                    resolve(response);
                });
            })
            .catch(error => {
                var response = this.handleError(error);

                return new Promise<any>((resolve, reject) => {
                    reject(response);
                });
            });
    }
}