﻿import { Injectable } from '@angular/core';
import { HttpClientService } from './http/httpclient.service';
import { Subject } from 'rxjs/Subject';

import { NotificationService } from './notification/notification.service';
import { User, Notification, NotificationType, Project } from './model/model';

@Injectable()
export class AppService {
    loginSource = new Subject();
    loginStatus$ = this.loginSource.asObservable();

    user: Subject<User> = new Subject();



    constructor(private httpservice: HttpClientService, private notificationService: NotificationService) {

    }

    getProjects(): Promise<Project[]> {
        return this.httpservice.get("api/Project")
            .then(response => {
                return JSON.parse(response['_body']) as Project[];
            })
            .catch(error => {
                return [];
            });
    }


    logout(): Promise<boolean> {

        return this.httpservice.post("api/auth/logout",null)
            .then(response => {
                this.deleteSession();
                return true;
            })
            .catch(error => {
                return false;
            });

    }

    deleteSession(): void {
        sessionStorage.removeItem('sessionId');
        //this.user = null;
    }


 
}


