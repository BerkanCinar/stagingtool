// Angular internals
import { NgModule, LOCALE_ID } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { HttpModule } from '@angular/http';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

// Components
import { PatientComponent } from './patient/patient.component';
import { NotificationComponent } from './notification/notification.component';
import { LoaderComponent } from './loader/loader.component';
import { ProjectComponent } from './project/project.component';
import { LoginComponent } from './login/login.component';
import { AdminComponent } from './admin/admin.component';

// Modules
import { AppRoutingModule } from './app-routing.module';

// Services
import { LoaderService } from './loader/loader.service';
import { HttpClientService } from './http/httpclient.service';
import { PatientService } from './patient/patient.service';
import { NotificationService } from './notification/notification.service';
import { ProjectService } from './project/project.service';
import { LoginService } from './login/login.service';
import { AppService } from './app.service';
import { AuthGuard } from './routeAuth/auth-guard.service';


// Modals
import { ProjectComponentModal } from './project/modal/project.modal.component';
import { PatientComponentModal } from "./patient/modal/patient.modal.component";
import { LoginComponentModal } from "./login/modal/login.modal.component";



// Externals
import { CalendarModule } from 'primeng/primeng';
import { MultiselectDropdownModule } from 'angular-2-dropdown-multiselect';
//import { NgxDatatableModule } from '@swimlane/ngx-datatable';
//import { CovalentLayoutModule, CovalentStepsModule } from '@covalent/core';


// Admin
import { AdminUsersComponent } from './admin/users/admin.users.component';
import { AdminDepartmentComponent } from './admin/departments/admin.department.component';

import { AdminService } from './admin/admin.service';
import { AdminUsersComponentModal } from './admin/users/modal/admin.users.modal.component';
import { AdminDepartmentComponentModal } from './admin/departments/modal/admin.department.modal.component';


@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpModule,
        AppRoutingModule,
        CalendarModule,
        MultiselectDropdownModule
        //NgxDatatableModule,
        //CovalentLayoutModule,
        //CovalentStepsModule
    ],
    declarations: [
        // COMPONENTS
        AppComponent,
        PatientComponent,
        NotificationComponent,
        LoaderComponent,
        ProjectComponent,
        LoginComponent,
        AdminComponent,

        // MODALS
        PatientComponentModal,
        ProjectComponentModal,
        LoginComponentModal,
   

        // Admin
        AdminUsersComponent,
        AdminDepartmentComponentModal,
        AdminUsersComponentModal,
        AdminDepartmentComponent
    ],
    // Runtime generated Components (MODALS)
    entryComponents: [
        ProjectComponentModal,
        LoginComponentModal,
        AdminDepartmentComponentModal,

        // ADMIN
        AdminUsersComponentModal
    ],
    bootstrap: [AppComponent],
    providers: [
        // Services
        LoaderService,
        HttpClientService,
        PatientService,
        AppService,
        LoginService,
        NotificationService,
        ProjectService,
        AuthGuard,
        // ADMIN
        AdminService,
        { provide: LOCALE_ID, useValue: "de-DE" }]
})
export class AppModule { }
