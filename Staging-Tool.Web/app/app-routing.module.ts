import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientComponent } from './patient/patient.component';
import { ProjectComponent } from './project/project.component';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './routeAuth/auth-guard.service';

const routes: Routes = [

    { path: 'patient', component: PatientComponent, canActivate:[AuthGuard] },
    { path: 'project', component: ProjectComponent, canActivate:[AuthGuard] },
    { path: 'admin', component: AdminComponent, canActivate:[AuthGuard] },

    { path: 'login', component: LoginComponent },
    { path: '**', component: LoginComponent },
    { path: '', redirectTo: '/login', pathMatch: 'full' }

];
@NgModule({
    imports: [RouterModule.forRoot(routes, { useHash: true })], // , { useHash: true }
    exports: [RouterModule]
})
export class AppRoutingModule { }