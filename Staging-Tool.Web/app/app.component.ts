import { Component, OnInit } from '@angular/core';
import { AppService } from './app.service';
import { NotificationService } from './notification/notification.service';
import { Notification, NotificationType, User } from './model/model';
import { Router } from '@angular/router';

@Component({
    moduleId: module.id.toString(),
    selector: 'my-app',
    templateUrl: './app.component.html', 
})
export class AppComponent implements OnInit {
    user: User;

    constructor(private appService: AppService, private notificationService: NotificationService, private router: Router) {

        appService.user.subscribe((user: User) => {
            this.user = user;
        })
    }

    private logged: boolean = false;

    ngOnInit(): void {
        this.appService.loginStatus$.subscribe(loginStatus => {
            this.logged = <boolean>loginStatus;
        })
    }


    logout(): void {
        this.appService.logout().then(success => {
            if (success) {
                this.appService.loginSource.next(false);
                this.router.navigate(["/login"]);
            } else {
                this.notificationService.note.next(new Notification("Logout derzeit nicht m�glich!", NotificationType.ERROR));
            }
        })
    }

}