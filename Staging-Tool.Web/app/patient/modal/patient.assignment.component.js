"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var model_1 = require("../../model/model");
var app_service_1 = require("../../app.service");
var notification_service_1 = require("../../notification/notification.service");
var project_service_1 = require("../../project/project.service");
var PatientComponentAssignment = (function () {
    function PatientComponentAssignment(projectService, globalService, notificationService) {
        this.projectService = projectService;
        this.globalService = globalService;
        this.notificationService = notificationService;
        this.refresh();
    }
    PatientComponentAssignment.prototype.refresh = function () {
        var _this = this;
        this.projectService.getProjects().then(function (projects) {
            _this.projects = projects;
        });
    };
    return PatientComponentAssignment;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", model_1.Patient)
], PatientComponentAssignment.prototype, "selectedPatient", void 0);
PatientComponentAssignment = __decorate([
    core_1.Component({
        moduleId: module.id.toString(),
        selector: 'app-patient-assignment',
        templateUrl: './patient.assignment.component.html',
        styleUrls: ['../patient.component.css']
    }),
    __metadata("design:paramtypes", [project_service_1.ProjectService, app_service_1.AppService, notification_service_1.NotificationService])
], PatientComponentAssignment);
exports.PatientComponentAssignment = PatientComponentAssignment;
//# sourceMappingURL=patient.assignment.component.js.map