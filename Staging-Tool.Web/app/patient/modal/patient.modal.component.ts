﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import {
    Notification, NotificationType, Project, Patient,
    PatientInProject, ModalClose, ModalCancelReason, PatientDestination
} from '../../model/model';



@Component({
    moduleId: module.id.toString(),
    selector: 'app-patient-modal',
    templateUrl: './patient.modal.component.html',
    styleUrls: ['../patient.component.css']
})

export class PatientComponentModal {

    @Input() data: PatientInProject[];
    @Output() cancel = new EventEmitter<ModalClose>();
    patientDestination: typeof PatientDestination = PatientDestination;



    constructor() {}

    submit(): void {
        let destinationSelected = this.data.every(el => el.destination != null)
        if (destinationSelected) {
            var close = new ModalClose();
            close.Type = ModalCancelReason.SUBMIT;
            close.Obj = this.data;
            this.cancel.emit(close);
            this.data = null;
        } else {
            alert('Bitte wählen Sie für jeden Patienten das Ziel aus!')
        }
    }

    // ++++++++++++++++++++++++++++++++++++++++++
    // BACKUP CODE FOR PATIENTDESTIONATION 
    // ++++++++++++++++++++++++++++++++++++++++++
    //destinationChanged(destination: PatientDestination, reference_id: number, case_id: number): void {

    //    if (destination === PatientDestination.NEW) {

    //        // referenzen suchen
    //        let tmp = this.data.filter(el => el.reference_id === reference_id);

    //        // enable instance and disable new patient selection
    //            tmp.forEach(el => {
    //                if (el.hospital_case.case_id_int != case_id) {
    //                    el.enableNewPatient = false;
    //                    el.enableInstance = true;
    //                }
    //            })


    //    } else if (destination === PatientDestination.IGNORE) {
    //        var tmp = this.data.filter(el => el.reference_id === reference_id);

    //        // get selections with NEW Destination
    //        var newDestinationForRefGroup = tmp.filter(el => el.destination === PatientDestination.NEW);

    //        // check if there is a NEW value
    //        if (newDestinationForRefGroup.length > 0) {

    //            // there should be only 1 NEW Destination in this reference group!

    //            tmp.forEach(el => {
    //                if (newDestinationForRefGroup[0].hospital_case.case_id_int != el.hospital_case.case_id_int) {
    //                    // er darf das instanziieren aber NUR weil ein neuer Patient in der Auswahlliste ist! Er dürfte aber auch instanziieren wenn einer new ist aber nicht 
    //                    el.enableInstance = true;
    //                    el.enableNewPatient = false;
    //                    //el.patientInProject = true;
    //                }
    //            })
               
    //        } else {
    //            // no NEW destinations in reference group found --> disable already selected instances and set Instance to false
           
    //            if (tmp.some(el => el.patientInProject)) {
    //                tmp.forEach(el => {
    //                    //el.patientInProject = false;
                        
    //                    el.enableInstance = true;
    //                    el.enableNewPatient = true;
    //                    //if (el.destination === PatientDestination.INSTANCE)
    //                    //    el.destination = null;

                            

    //                })
    //            } else {
    //                tmp.forEach(el => {
    //                    el.enableNewPatient = true;
    //                    el.enableInstance = false;

    //                    if (el.destination === PatientDestination.INSTANCE)
    //                        el.destination = null;
                       
    //                })
    //            }
    //        }

    //    } else if (destination === PatientDestination.INSTANCE) {
    //        let tmp = this.data.filter(el => el.reference_id === reference_id);

    //        var destinationNewInArray = tmp.some(el => el.destination === PatientDestination.NEW);
    //        // check if there is a patient in db OR a patient with destionation new in list
    

    //        if (destinationNewInArray || tmp.some(el => el.patientInProject)) {
    //            tmp.forEach(el => {
    //                //el.patientInProject = false;
    //                if (el.destination != PatientDestination.NEW) {
    //                    el.enableInstance = true;
    //                    el.enableNewPatient = false;

    //                }
                    
    //                if (!destinationNewInArray) {
    //                    el.enableNewPatient = true;
    //                } 
    //            })
    //        } else {
    //            tmp.forEach(el => {
    //                el.enableNewPatient = true;
    //                el.enableInstance = false;

    //                if (el.destination === PatientDestination.INSTANCE)
    //                    el.destination = null;

    //            });
    //        }
    //    }  
    //}


    destinationChanged(destination: PatientDestination, reference_id: number, case_id: number): void {

        if (destination === PatientDestination.NEW) {

            // referenzen suchen
            let tmp = this.data.filter(el => el.reference_id === reference_id);

            // enable instance and disable new patient selection
            tmp.forEach(el => {
                if (el.hospital_case.case_id_int != case_id) {
                    el.newRecord = false;
                    el.listInstance = true;
                }
            })


        } else if (destination === PatientDestination.IGNORE) {
            var tmp = this.data.filter(el => el.reference_id === reference_id);

            // get selections with NEW Destination
            var newDestinationForRefGroup = tmp.filter(el => el.destination === PatientDestination.NEW);

            // check if there is a NEW value
            if (newDestinationForRefGroup.length == 0) {

                tmp.forEach(el => {
                    el.listInstance = false;
                    el.newRecord = true;

                    if (el.destination === PatientDestination.INSTANCE && !el.instance)
                        el.destination = null;
                })

            }

        } else if (destination === PatientDestination.INSTANCE) {
            let tmp = this.data.filter(el => el.reference_id === reference_id);

            var destinationNewInArray = tmp.some(el => el.destination === PatientDestination.NEW);

            if (!destinationNewInArray) {
                tmp.forEach(el => {
                        el.listInstance = false;
                        el.newRecord = true;
                        if (el.destination === PatientDestination.INSTANCE && !el.instance)
                            el.destination = null;
                })
            } 
        }
    }

    cancelForm(): void {
        var close = new ModalClose();
        close.Type = ModalCancelReason.CANCEL;

        this.cancel.emit(close);
        this.data = null;
    }
        
}