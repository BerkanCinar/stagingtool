﻿import { Component } from '@angular/core';
import { PatientService } from './patient.service';
import { Http, Response } from '@angular/http';
import { NotificationService } from '../notification/notification.service';
import { AppService } from '../app.service';
import 'rxjs/Rx';
import { Notification, NotificationType, Project, Case, PatientInProject, ModalClose, ModalCancelReason, PatientDestination } from '../model/model';


@Component({
    moduleId: module.id.toString(),
    selector: 'app-patient',
    templateUrl: './patient.component.html',
    styleUrls: ['./patient.component.css']
})

export class PatientComponent {

    private projects: Project[];
    private selectedProjectIDs: number[];
    private selectedCaseIDs: number[];
    private destinationOfSelectedData: PatientInProject[];

    // orginal array
    private cases: Case[];
    // filtered array
    private filteredCases: Case[];
    private filteredProjects: Project[];
    private filterType: String[];
    private defaultFilterType: string;

    // Pageing
    private showableCases: Case[];
    private caseCount: number;
    private filterdCaseCount: number;
    private rowsPerPage: number;
    private rowsPerPageArr: String[];
    private rowsPerPageStr: String;
    private pagescount: number;
    private page: number;
    private pages: number[];

    private selectedCaseCount: number;
    private expiredCaseCount: number;
    private showDischarged: boolean;
    private showExpired: boolean;
    private hideFutureAdmission: boolean;
    private patHeadline: string;

    // Var zur Datenfilterung
    fallartFilterString: string
    fallFilterString: string;
    patientFilterString: string;
    nameFilterString: string;
    vornameFilterString: string;
    stationFilterString: string;
    stationAktuellFilterString: string;
    abteilungFilterString: string;
    abteilungAktuellFilterString: string;
    aufnahmeFilterString: string;
    projekteFilterString: string;
    gebdatFilterString: string;
    alterFilterString: string;
    geschlechtFilterString: string;
    plzFilterString: string;
    letzteNachrichtFilterString: string;

    fallartFilterType: string;
    nameFilterType: string;
    vornameFilterType: string;
    abteilungAktuellFilterType: string;
    stationAktuellFilterType: string;
    abteilungFilterType: string;
    stationFilterType: string;



    projectIdFilterString: string;
    projectNameFilterString: string;

    isDesc: boolean;
    sortCol: number;
    
    private temp: any[];
    private tempProjects: any[];


    constructor(private patientService: PatientService, private globalService: AppService, private notificationService: NotificationService) {
        this.rowsPerPageArr = ["10", "20", "30", "50", "100", "200", "500", "∞"];
        this.filterType = ["enthält", "beginnt mit"];
        this.patHeadline = "AKTIVE Fälle";
        this.showDischarged = false;
        this.hideFutureAdmission = false;
        this.showExpired = false;
        this.refresh(this.showDischarged);
    }

    
    refresh(showDischarged: boolean): void {
        this.destinationOfSelectedData = null
        this.selectedCaseIDs = [];
        this.selectedCaseCount = 0;
        this.selectedProjectIDs = [];

        this.backToPatients();


        this.patientService.getCases(this.showDischarged).then(cases => {
            this.cases = cases;
            this.caseCount = this.cases.length;
            this.filteredCases = this.cases;
            this.isDesc = false;
            this.sortCol = 0;
            this.rowsPerPage = 100;
            this.rowsPerPageStr = "100";
            this.prepareFiltertypes();
            this.filterCases(null);
            this.preparePager();
            this.buildCasePage(1);
        });

    }

    onRefreshClick() {
        //this.refresh(this.showDischarged);
        this.patientService.getCases(this.showDischarged).then(cases => {
            this.cases = cases;
            this.caseCount = this.cases.length;
            this.filteredCases = this.cases;
            this.isDesc = false;
            this.sortCol = 0;
            //this.rowsPerPage = 100;
            //this.rowsPerPageStr = "100";
            //this.prepareFiltertypes();
            this.filterCases(null);
            this.preparePager();
            this.buildCasePage(1);
        });
        this.showExpired = this.showDischarged;
        if (this.showDischarged == true) {
            this.patHeadline = "INAKTIVE Fälle";
        } else {
            this.patHeadline = "AKTIVE Fälle";
        }
    }


    filterFutureAdmission() {
        if (this.hideFutureAdmission == true) {

        }
    }



    onRowsPerPageChange(event: any) {
        if (event == "∞") {
            this.rowsPerPage = this.cases.length;
        } else {
            this.rowsPerPage = parseInt(event);
        }
        
        this.preparePager();
        this.buildCasePage(1);
    }


    // Case Filter & Sort
    prepareFiltertypes() {
        this.defaultFilterType = "enthält";

        this.fallartFilterType = "enthält";
        this.nameFilterType = "enthält";
        this.vornameFilterType = "enthält";
        this.abteilungAktuellFilterType = "enthält";
        this.stationAktuellFilterType = "enthält";
        this.abteilungFilterType = "enthält";
        this.stationFilterType = "enthält";
    }


    onFilterTypeChange(event: any, spalte: string) {
        switch (spalte) {
            case "fallart": {
                this.fallartFilterType = event;
                break;
            }
            case "name": {
                this.nameFilterType = event;
                break;
            }
            case "vorname": {
                this.vornameFilterType = event;
                break;
            }
            case "abteilungAktuell": {
                this.abteilungAktuellFilterType = event;
                break;
            }
            case "stationAktuell": {
                this.stationAktuellFilterType = event;
                break;
            }
            case "abteilung": {
                this.abteilungFilterType = event;
                break;
            }
            case "station": {
                this.stationFilterType = event;
                break;
            }
        }
    }



    filterCases(event: any) {
        // filterwerte
        var fallart = this.fallartFilterString;
        var fall = this.fallFilterString;
        var patient = this.patientFilterString;
        var name = this.nameFilterString;
        var vorname = this.vornameFilterString;
        var station = this.stationFilterString;
        var stationAktuell = this.stationAktuellFilterString;
        var abteilung = this.abteilungFilterString;
        var abteilungAktuell = this.abteilungAktuellFilterString;
        var aufnahme = this.aufnahmeFilterString;
        var projekte = this.projekteFilterString;
        var gebdat = this.gebdatFilterString;
        var alter = this.alterFilterString;
        var geschlecht = this.geschlechtFilterString;
        var plz = this.plzFilterString;
        var letzteNachricht = this.letzteNachrichtFilterString;

        // zu durchsuchende datensätze (alle)
        this.temp = this.cases;

        // number
        if (fall != null && !(fall.trim() == "<" || fall.trim() == ">" || fall.trim() == "=")) {
            if (fall.startsWith("<")) {
                this.temp = this.temp.filter(function (x) {
                    return (x.case_id_ext == null) ? false : parseInt(x.case_id_ext) < parseInt(fall.substring(1).trim());
                });
            } else if (fall.startsWith(">")) {
                this.temp = this.temp.filter(function (x) {
                    return (x.case_id_ext == null) ? false : parseInt(x.case_id_ext) > parseInt(fall.substring(1).trim());
                });
            } else if (fall.startsWith("=")) {
                this.temp = this.temp.filter(function (x) {
                    return (x.case_id_ext == null) ? false : parseInt(x.case_id_ext) == parseInt(fall.substring(1).trim());
                });
            } else {
                this.temp = this.temp.filter(function (x) {
                    return (!fall || x.case_id_ext.toString().toLowerCase().startsWith(fall.toLowerCase()));
                });
            }
        }

        if (patient != null && !(patient.trim() == "<" || patient.trim() == ">" || patient.trim() == "=")) {
            if (patient.startsWith("<")) {
                this.temp = this.temp.filter(function (x) {
                    return (x.patient.patient_id_ext == null) ? false : parseInt(x.patient.patient_id_ext) < parseInt(patient.substring(1).trim());
                });
            } else if (patient.startsWith(">")) {
                this.temp = this.temp.filter(function (x) {
                    return (x.patient.patient_id_ext == null) ? false : parseInt(x.patient.patient_id_ext) > parseInt(patient.substring(1).trim());
                });
            } else if (patient.startsWith("=")) {
                this.temp = this.temp.filter(function (x) {
                    return (x.patient.patient_id_ext == null) ? false : parseInt(x.patient.patient_id_ext) == parseInt(patient.substring(1).trim());
                });
            } else {
                this.temp = this.temp.filter(function (x) {
                    return (!patient || x.patient.patient_id_ext.toString().toLowerCase().startsWith(patient.toLowerCase()));
                });
            }
        }

        if (alter != null && !(alter.trim() == "<" || alter.trim() == ">" || alter.trim() == "=")) {
            if (alter.startsWith("<")) {
                this.temp = this.temp.filter(function (x) {
                    return (x.patient.alter == null) ? false : parseInt(x.patient.alter) < parseInt(alter.substring(1).trim());
                });
            } else if (alter.startsWith(">")) {
                this.temp = this.temp.filter(function (x) {
                    return (x.patient.alter == null) ? false : parseInt(x.patient.alter) > parseInt(alter.substring(1).trim());
                });
            } else if (alter.startsWith("=")) {
                this.temp = this.temp.filter(function (x) {
                    return (x.patient.alter == null) ? false : parseInt(x.patient.alter) == parseInt(alter.substring(1).trim());
                });
            } else {
                this.temp = this.temp.filter(function (x) {
                    return (!alter || x.patient.alter.toString().toLowerCase().startsWith(alter.toLowerCase()));
                });
            }
        }

        // Datum
        if (aufnahme != null && !(aufnahme.trim() == "<" || aufnahme.trim() == ">" || aufnahme.trim() == "=")) {
            var dmy = aufnahme.substring(1).trim().split(".");
            var d = new Date(parseInt(dmy[2]), parseInt(dmy[1]) - 1, parseInt(dmy[0]));

            if (aufnahme.startsWith("<")) {
                this.temp = this.temp.filter(function (x) {
                    //return x.patient.date_of_birth < d;
                    return (x.case_start == null) ? false : new Date(parseInt(x.case_start.substring(0, 10).split("-")[0]), parseInt(x.case_start.substring(0, 10).split("-")[1]) - 1, parseInt(x.case_start.substring(0, 10).split("-")[2])) < d;
                });
            } else if (aufnahme.startsWith(">")) {
                this.temp = this.temp.filter(function (x) {
                    //return x.patient.date_of_birth > d;
                    return (x.case_start == null) ? false : new Date(parseInt(x.case_start.substring(0, 10).split("-")[0]), parseInt(x.case_start.substring(0, 10).split("-")[1]) - 1, parseInt(x.case_start.substring(0, 10).split("-")[2])) > d;
                });
            } else if (aufnahme.startsWith("=")) {
                this.temp = this.temp.filter(function (x) {
                    //return x.patient.date_of_birth == d;
                    return (x.case_start == null) ? false : new Date(parseInt(x.case_start.substring(0, 10).split("-")[0]), parseInt(x.case_start.substring(0, 10).split("-")[1]) - 1, parseInt(x.case_start.substring(0, 10).split("-")[2])).getTime() == d.getTime();
                });
            } else {
                this.temp = this.temp.filter(function (x) {
                    return (!aufnahme || (x.case_start != null && x.case_start_str.toString().toLowerCase().indexOf(aufnahme.toLowerCase()) > -1));
                });
            }
        }

        if (gebdat != null && !(gebdat.trim() == "<" || gebdat.trim() == ">" || gebdat.trim() == "=")) {
            var dmy = gebdat.substring(1).trim().split(".");
            var d = new Date(parseInt(dmy[2]), parseInt(dmy[1]) - 1, parseInt(dmy[0]));

            if (gebdat.startsWith("<")) {
                this.temp = this.temp.filter(function (x) {
                    //return x.patient.date_of_birth < d;
                    return (x.patient.date_of_birth == null) ? false : new Date(parseInt(x.patient.date_of_birth.substring(0, 10).split("-")[0]), parseInt(x.patient.date_of_birth.substring(0, 10).split("-")[1]) - 1, parseInt(x.patient.date_of_birth.substring(0, 10).split("-")[2])) < d;
                });
            } else if (gebdat.startsWith(">")) {
                this.temp = this.temp.filter(function (x) {
                    //return x.patient.date_of_birth > d;
                    return (x.patient.date_of_birth == null) ? false : new Date(parseInt(x.patient.date_of_birth.substring(0, 10).split("-")[0]), parseInt(x.patient.date_of_birth.substring(0, 10).split("-")[1]) - 1, parseInt(x.patient.date_of_birth.substring(0, 10).split("-")[2])) > d;
                });
            } else if (gebdat.startsWith("=")) {
                this.temp = this.temp.filter(function (x) {
                    //return x.patient.date_of_birth == d;
                    return (x.patient.date_of_birth == null) ? false : new Date(parseInt(x.patient.date_of_birth.substring(0, 10).split("-")[0]), parseInt(x.patient.date_of_birth.substring(0, 10).split("-")[1]) - 1, parseInt(x.patient.date_of_birth.substring(0, 10).split("-")[2])).getTime() == d.getTime();
                });
            } else {
                this.temp = this.temp.filter(function (x) {
                    return (!gebdat || (x.patient.date_of_birth != null && x.patient.date_of_birth_str.toString().toLowerCase().indexOf(gebdat.toLowerCase()) > -1));
                });
            }
        }

        if (letzteNachricht != null && !(letzteNachricht.trim() == "<" || letzteNachricht.trim() == ">" || letzteNachricht.trim() == "=")) {
            var dmy = letzteNachricht.substring(1).trim().split(".");
            var d = new Date(parseInt(dmy[2]), parseInt(dmy[1]) - 1, parseInt(dmy[0]));

            if (letzteNachricht.startsWith("<")) {
                this.temp = this.temp.filter(function (x) {
                    //return x.patient.date_of_birth < d;
                    return (x.case_last_message == null) ? false : new Date(parseInt(x.case_last_message.substring(0, 10).split("-")[0]), parseInt(x.case_last_message.substring(0, 10).split("-")[1]) - 1, parseInt(x.case_last_message.substring(0, 10).split("-")[2])) < d;
                });
            } else if (letzteNachricht.startsWith(">")) {
                this.temp = this.temp.filter(function (x) {
                    //return x.patient.date_of_birth > d;
                    return (x.case_last_message == null) ? false : new Date(parseInt(x.case_last_message.substring(0, 10).split("-")[0]), parseInt(x.case_last_message.substring(0, 10).split("-")[1]) - 1, parseInt(x.case_last_message.substring(0, 10).split("-")[2])) > d;
                });
            } else if (letzteNachricht.startsWith("=")) {
                this.temp = this.temp.filter(function (x) {
                    //return x.patient.date_of_birth == d;
                    return (x.case_last_message == null) ? false : new Date(parseInt(x.case_last_message.substring(0, 10).split("-")[0]), parseInt(x.case_last_message.substring(0, 10).split("-")[1]) - 1, parseInt(x.case_last_message.substring(0, 10).split("-")[2])).getTime() == d.getTime();
                });
            } else {
                this.temp = this.temp.filter(function (x) {
                    return (!letzteNachricht || (x.case_last_message != null && x.case_last_message_str.toString().toLowerCase().indexOf(letzteNachricht.toLowerCase()) > -1));
                });
            }
        }


        //Fallart
        if (this.fallartFilterType == "enthält") {
            this.temp = this.temp.filter(function (c) {
                return ((!fallart || c.case_kind.toString().toLowerCase().indexOf(fallart.toLowerCase()) > -1)
                );
            });
        } else {
            this.temp = this.temp.filter(function (c) {
                return ((!fallart || c.case_kind.toString().toLowerCase().startsWith(fallart.toLowerCase()))
                );
            });
        }

        //Name
        if (this.nameFilterType == "enthält") {
            this.temp = this.temp.filter(function (c) {
                return ((!name || c.patient.last_name.toString().toLowerCase().indexOf(name.toLowerCase()) > -1)
                );
            });
        } else {
            this.temp = this.temp.filter(function (c) {
                return ((!name || c.patient.last_name.toString().toLowerCase().startsWith(name.toLowerCase()))
                );
            });
        }

        //Vorname
        if (this.vornameFilterType == "enthält") {
            this.temp = this.temp.filter(function (c) {
                return ((!vorname || c.patient.first_name.toString().toLowerCase().indexOf(vorname.toLowerCase()) > -1)
                );
            });
        } else {
            this.temp = this.temp.filter(function (c) {
                return ((!vorname || c.patient.first_name.toString().toLowerCase().startsWith(vorname.toLowerCase()))
                );
            });
        }

        //Abteilung aktuell
        if (this.abteilungAktuellFilterType == "enthält") {
            this.temp = this.temp.filter(function (c) {
                return ((!abteilungAktuell || c.department_current.department_short_name.toString().toLowerCase().indexOf(abteilungAktuell.toLowerCase()) > -1)
                );
            });
        } else {
            this.temp = this.temp.filter(function (c) {
                return ((!abteilungAktuell || c.department_current.department_short_name.toString().toLowerCase().startsWith(abteilungAktuell.toLowerCase()))
                );
            });
        }

        //Station aktuell
        if (this.stationAktuellFilterType == "enthält") {
            this.temp = this.temp.filter(function (c) {
                return ((!stationAktuell || c.ward_current.ward_short_name.toString().toLowerCase().indexOf(stationAktuell.toLowerCase()) > -1)
                );
            });
        } else {
            this.temp = this.temp.filter(function (c) {
                return ((!stationAktuell || c.ward_current.ward_short_name.toString().toLowerCase().startsWith(stationAktuell.toLowerCase()))
                );
            });
        }

        //Abteilung
        if (this.abteilungFilterType == "enthält") {
            this.temp = this.temp.filter(function (c) {
                return ((!abteilung || c.department_admission.department_short_name.toString().toLowerCase().indexOf(abteilung.toLowerCase()) > -1)
                );
            });
        } else {
            this.temp = this.temp.filter(function (c) {
                return ((!abteilung || c.department_admission.department_short_name.toString().toLowerCase().startsWith(abteilung.toLowerCase()))
                );
            });
        }

        //Station
        if (this.stationFilterType == "enthält") {
            this.temp = this.temp.filter(function (c) {
                return ((!station || c.ward_admission.ward_short_name.toString().toLowerCase().indexOf(station.toLowerCase()) > -1)
                );
            });
        } else {
            this.temp = this.temp.filter(function (c) {
                return ((!station || c.ward_admission.ward_short_name.toString().toLowerCase().startsWith(station.toLowerCase()))
                );
            });
        }


        this.temp = this.temp.filter(function (c) {
            return ((!projekte || c.projects_str.toLowerCase().indexOf(projekte.toLowerCase()) > -1)
                && (!geschlecht || c.patient.gender.toLowerCase().indexOf(geschlecht.toLowerCase()) > -1)
                && (!plz || c.patient.zip_code.toLowerCase().indexOf(plz.toLowerCase()) > -1)
            );
        });

        //FutureAdmission
        if (this.hideFutureAdmission == true) {
            var now = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate() + 1);
            this.temp = this.temp.filter(function (x) {
                //return x.patient.date_of_birth < d;
                return (x.case_start == null) ? false : new Date(parseInt(x.case_start.substring(0, 10).split("-")[0]), parseInt(x.case_start.substring(0, 10).split("-")[1]) - 1, parseInt(x.case_start.substring(0, 10).split("-")[2])) < now;
            });
        }

        // gefilterte daten zurück in tabelle
        this.filteredCases = this.temp;

        this.preparePager();
        this.buildCasePage(1);
    }

    sortCases(property1: string, property2: string, sortCol: number) {
        this.sortCol = sortCol;
        this.isDesc = !this.isDesc; //change the direction    
        let direction = this.isDesc ? 1 : -1;

        if (property1 == "") {
            this.filteredCases.sort(function (obj1, obj2) {
                if (obj1[property2] > obj2[property2]) return 1 * direction;
                if (obj1[property2] < obj2[property2]) return -1 * direction;
                return 0;
            });
        } else if (property1 == "patient") {
            this.filteredCases.sort(function (obj1, obj2) {
                if (obj1.patient[property2] > obj2.patient[property2]) return 1 * direction;
                if (obj1.patient[property2] < obj2.patient[property2]) return -1 * direction;
                return 0;
            });
        } else if (property1 == "ward_admission") {
            this.filteredCases.sort(function (obj1, obj2) {
                if (obj1.ward_admission == null) return -1 * direction;
                if (obj2.ward_admission == null) return 1 * direction;
                if (obj1.ward_admission[property2] > obj2.ward_admission[property2]) return 1 * direction;
                if (obj1.ward_admission[property2] < obj2.ward_admission[property2]) return -1 * direction;
                return 0;
            });
        } else if (property1 == "ward_current") {
            this.filteredCases.sort(function (obj1, obj2) {
                if (obj1.ward_current == null) return -1 * direction;
                if (obj2.ward_current == null) return 1 * direction;
                if (obj1.ward_current[property2] > obj2.ward_current[property2]) return 1 * direction;
                if (obj1.ward_current[property2] < obj2.ward_current[property2]) return -1 * direction;
                return 0;
            });
        } else if (property1 == "department_admission") {
            this.filteredCases.sort(function (obj1, obj2) {
                if (obj1.department_admission == null) return -1 * direction;
                if (obj2.department_admission == null) return 1 * direction;
                if (obj1.department_admission[property2] > obj2.department_admission[property2]) return 1 * direction;
                if (obj1.department_admission[property2] < obj2.department_admission[property2]) return -1 * direction;
                return 0;
            });
        } else if (property1 == "department_current") {
            this.filteredCases.sort(function (obj1, obj2) {
                if (obj1.department_current == null) return -1 * direction;
                if (obj2.department_current == null) return 1 * direction;
                if (obj1.department_current[property2] > obj2.department_current[property2]) return 1 * direction;
                if (obj1.department_current[property2] < obj2.department_current[property2]) return -1 * direction;
                return 0;
            });
        } 

        this.buildCasePage(1);
    };

    preparePager() {
        if (this.filteredCases.length % this.rowsPerPage != 0) {
            this.pagescount = ~~((this.filteredCases.length / this.rowsPerPage) + 1);
        } else {
            this.pagescount = ~~((this.filteredCases.length / this.rowsPerPage));
        }
        this.pages = [];
        for (var i = 1; i <= this.pagescount; i++) {
            this.pages.push(i);
        }
    }

    buildCasePage(page: number) {
        this.showableCases = this.filteredCases.slice((page - 1) * this.rowsPerPage, (page - 1) * this.rowsPerPage + this.rowsPerPage)
        if (this.showDischarged == true) {
            this.expiredCaseCount = 0;
            this.filteredCases.forEach(el => {
                if (el.case_end == null)
                    this.expiredCaseCount++;
            })
        }
        this.filterdCaseCount = this.filteredCases.length;
        this.page = page;
    }

    
    switchCasePage(direction: string) {
        if (direction == "next") {
            if (!(this.page + 1 > this.pagescount)) {
                this.buildCasePage(this.page + 1);
            }
        }
        else if (direction == "prev") {
            if (!(this.page - 1 == 0)) {
                this.buildCasePage(this.page - 1);
            }
        }
        else if (direction == "last") {
            this.buildCasePage(this.pages.length);
        }
        else if (direction == "first") {
            this.buildCasePage(1);
        }
    }
    
    onPageChange(event: any) {
        this.buildCasePage(parseInt(event));
    }


    selectAllCases(event: any) {
        const val = event.target.checked;

        for (let hcase of this.filteredCases) {
            hcase.selected = val
        }

        if (val == true) {
            this.selectedCaseCount = this.filteredCases.length;
        } else {
            this.selectedCaseCount = 0;
        }
        
    }

    updateSelectedCaseCounter(event: any, clickedCase: Case) {
        if (event.target.type == "checkbox") {
            if (event.target.checked == true) {
                this.selectedCaseCount++;
            } else {
                this.selectedCaseCount--;
            }
        } else {
            if (clickedCase.selected == true) {
                this.selectedCaseCount++;
            } else {
                this.selectedCaseCount--;
            }
        }
        
    }

    getTableStyle(i: number) {
        if (i % 2 == 0) {
            return "#D0F6D2";
        } else {
            return "#CCE0CC";
        }
    }


    // Projects Filter & Sort

    filterProjects() {
        // filterwerte
        var id = this.projectIdFilterString;
        var name = this.projectNameFilterString;

        // zu durchsuchende datensätze (alle)
        this.tempProjects = this.projects;

        // filter
        const temp = this.tempProjects.filter(function (p) {
            return ((!id || p.project_id_ext.toString().toLowerCase().indexOf(id.toLowerCase()) > -1)
                && (!name || p.project_name.toLowerCase().indexOf(name.toLowerCase()) > -1)
                );
        });

        // gefilterte daten zurück in tabelle
        this.filteredProjects = temp;

        // whenever the filter changes, always go back to the first page
        //this.table.offset = 0;
    }


    sortProjects(property: string) {
        this.isDesc = !this.isDesc; //change the direction    
        let direction = this.isDesc ? 1 : -1;

        this.filteredProjects.sort(function (obj1, obj2) {
            if (obj1[property] > obj2[property]) return 1 * direction;
            if (obj1[property] < obj2[property]) return -1 * direction;
            return 0;
        });
    };


    



 
    patientDestinations(data: PatientInProject[]): void {

        this.patientService.setPatientsInProject(data).then(success => {
            if (success) {
                this.notificationService.note.next(new Notification("Zuordnung erfolgreich!", NotificationType.SUCCESS));
                this.destinationOfSelectedData = null;
                this.refresh(this.showDischarged);
            } else {
                this.notificationService.note.next(new Notification("Zuordnung fehlgeschlagen!", NotificationType.ERROR));
            }
        })

    }
 
    selectProjects(): void {


        this.projectIdFilterString = "";
        this.projectNameFilterString = "";

        this.cases.forEach(el => {
            if (el.selected)
                this.selectedCaseIDs.push(el.case_id_int);
        })


        if (this.selectedCaseIDs.length > 0) {
            this.globalService.getProjects().then(projects => {
                this.projects = projects;
                this.filteredProjects = projects;
                if (this.projects.length <= 0) {
                    this.notificationService.note.next(new Notification("Sie besitzen keine Berechtigungen auf ein Projekt", NotificationType.WARN));
                } else {
                    //this.sortProjectName("asc");
                }
            });

        } else {
            this.notificationService.note.next(new Notification("Kein(e) Patient(en) ausgewählt!", NotificationType.ERROR));
        }
    }


    backToPatients(): void {
        this.selectedProjectIDs = [];
        this.selectedCaseIDs = [];
        this.projects = null;
        this.filteredProjects = null;

    }


    formClosed(obj: ModalClose): void {
        switch (obj.Type) {
            case ModalCancelReason.CANCEL: this.destinationOfSelectedData = null;
                break;
            case ModalCancelReason.SUBMIT: this.patientDestinations(obj.Obj);
                break;
        }
    }




    setPatientsInProject(): void {
        this.selectedProjectIDs = [];
        this.filteredProjects.forEach(el => {
            if (el.selected) {
                this.selectedProjectIDs.push(el.project_id_int);
            }
        });


      
        
        if (this.selectedProjectIDs.length > 0) {
            this.patientService.checkIfPatientInProject(this.selectedProjectIDs, this.selectedCaseIDs).then(result => {

                // soriterungen vornehmen !

                // wenn ein referenz element sich ändert dann setztne wir den state um

                // BACKUP CODE FOR PATIENTDESTINATION
                //result.forEach(el => {
                //    if (el.caseInProject) {
                //        el.destination = PatientDestination.IGNORE;
                //    } else if (!el.caseInProject && el.patientInProject) {
                //        el.destination = PatientDestination.INSTANCE;
                //        el.enableInstance = true;
                //        el.enableNewPatient = true;
                //    } else if (!el.caseInProject && !el.patientInProject) {
                //        el.destination = null;
                //        el.enableNewPatient = true;
                //    }
                //})


                result.forEach(el => {
                    if (el.ignore) {
                        el.destination = PatientDestination.IGNORE;
                    } else if (el.instance || el.listInstance) {
                        el.destination = PatientDestination.INSTANCE;
                    } else if (el.newRecord) {
                        el.destination = PatientDestination.NEW;
                    }
                })

                this.destinationOfSelectedData = result;
                })

            } else {
                this.notificationService.note.next(new Notification("Kein(e) Projekt(e) ausgewählt!", NotificationType.ERROR));
            }

    }

}