﻿import { Injectable } from '@angular/core';
import { HttpClientService } from '../http/httpclient.service';
import {  Project, Case, Patient, PatientInProject } from '../model/model';

@Injectable()
export class PatientService {

    constructor(private httpservice: HttpClientService) {
    }


    getCases(showDischarged: boolean): Promise<Case[]> {
        //return this.httpservice.get("api/Case")
        //    .then(response => {
        //        return JSON.parse(response['_body']) as Case[];
        //    })
        //    .catch(error => {
        //        return [];
        //    });
        
        return this.httpservice.post("api/Case", { showDischarged })
            .then(response => {
                return JSON.parse(response['_body']) as Case[];
            })
            .catch(error => {
                return [];
            });
    }


    getProjects(): Promise<Project[]> {
        return this.httpservice.get("api/Project")
            .then(response => {
                return JSON.parse(response['_body']) as Project[];
            })
            .catch(error => {
                return [];
            });
    }

    checkIfPatientInProject(projectIDs: number[], caseIDs: number[]): Promise<PatientInProject[]> {

             
        return this.httpservice.post("api/PatientInProject", { projectIDs: projectIDs, caseIDs: caseIDs })
            .then(response => {
                return JSON.parse(response['_body']) as PatientInProject[];
            })
            .catch(error => {
                return [];
            });
    }

    setPatientsInProject(patientenData: PatientInProject[]): Promise<boolean> {


        return this.httpservice.post("api/SetPatientToRedCap" , { patientDestinationData: patientenData })
            .then(response => {
                return true;
            })
            .catch(error => {
                return false;
            });
    }
}