﻿import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { UserRight, Department, Ward, Project, ModalCancelReason, ModalClose } from '../../../model/model';

import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';

import { AdminService } from '../../admin.service';

@Component({
    moduleId: module.id.toString(),
    selector: 'app-admin-users-modal',
    templateUrl: './admin.users.modal.component.html',
    styleUrls: ['../admin.users.component.css']
})

export class AdminUsersComponentModal implements OnInit {

    private user: UserRight;
    private departmentWithWards: Department[];

    private oldDepartmentSelection: number[] = [];




    @Input() set newUser(value: UserRight) {

        if (value) {

            if (value.user.user_valid_from) {
                value.user.user_valid_from = new Date(value.user.user_valid_from);
            }

            if (value.user.user_valid_to) {
                value.user.user_valid_to = new Date(value.user.user_valid_to);
            }

            this.user = value;

            this.selectedWardOptions = [];
            this.selectedProjectOptions = [];
            this.selectedDepartmentOptions = [];
            this.oldDepartmentSelection = [];

            this.user.departments.forEach(el => {
                this.selectedDepartmentOptions.push(el.department_id);
            });

            this.user.projects.forEach(el => {
                this.selectedProjectOptions.push(el.project_id_int);
            });

            this.user.wards.forEach(el => {
                this.selectedWardOptions.push(el.ward_id_int);
            });


            this.departmentSelectionChanged();
        }

    }

    //@Input() user: UserRight;
    @Output() submit = new EventEmitter<UserRight>();
    //@Output() deleted = new EventEmitter<number>();
    @Output() cancel = new EventEmitter<ModalClose>();

    private de: any;

    // Permissions
    private wardOptions: IMultiSelectOption[];
    private departmentOptions: IMultiSelectOption[];
    private projectOptions: IMultiSelectOption[];

    private selectedWardOptions: number[];
    private selectedDepartmentOptions: number[];
    private selectedProjectOptions: number[];


    // Settings configuration
    private selectboxOptions: IMultiSelectSettings;

    // Text configuration
    private selectboxText: IMultiSelectTexts;

    ngOnInit() {
        this.de = {
            firstDayOfWeek: 1,
            dayNames: ["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"],
            dayNamesShort: ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"],
            dayNamesMin: ["M", "D", "M", "D", "F", "S", "S"],
            monthNames: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
            monthNamesShort: ["Jan", "Feb", "März", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
            today: 'Heute',
            clear: 'Zurücksetzten'
        };

        this.selectboxOptions = {
            enableSearch: true,
            checkedStyle: 'fontawesome',
            buttonClasses: 'btn btn-default btn-block',
            dynamicTitleMaxItems: 0,
            displayAllSelectedText: true,
            itemClasses: 'dropdownItem'
        };

        this.selectboxText = {
            checkAll: 'Alle',
            uncheckAll: 'Keine',
            checked: 'Element ausgewählt',
            checkedPlural: 'Elemente ausgewählt',
            defaultTitle: 'Auswählen',
            allSelected: 'Alle selektiert',
            searchPlaceholder: 'Suche...'

        };

    }

    constructor(private adminService: AdminService) {

        adminService.getPermissions().then(permission => {
            this.departmentOptions = [];
            this.projectOptions = [];
            this.wardOptions = [];


            this.selectedWardOptions = [];
            this.selectedProjectOptions = [];
            this.selectedDepartmentOptions = [];

            this.departmentWithWards = permission.departments;

            permission.departments.forEach(el => {
                let name = el.department_name ? el.department_name : ''
                    + el.department_short_name ? (" " + el.department_short_name) : '';

                this.departmentOptions.push({ id: el.department_id, name: name }) // el.department_name + " " + el.department_short_name
            });




            permission.projects.forEach(el =>
                this.projectOptions.push({ id: el.project_id_int, name: el.project_name })
            );

            permission.wards.forEach(el => {
                let name = el.ward_name ? el.ward_name : ''
                    + el.ward_short_name ? (" " + el.ward_short_name) : '';
                this.wardOptions.push({ id: el.ward_id_int, name: name }) // el.ward_name + " " + el.ward_short_name 
            }
            );

        });
    }

    resetPasswort(): void {
        if (confirm("Sind Sie sich sicher, dass Sie das Passwort vom Benutzer " + this.user.user.user_name + " zurücksetzten wollen?")) {

            var close = new ModalClose();
            close.Type = ModalCancelReason.RESET;
            close.Id = this.user.user.user_id;

            this.cancel.emit(close);
            this.user = null;
        }
    }

    addUser(): void {
        // object wieder zusammen basteln
        if (this.user.user.user_valid_from && this.user.user.user_name) {

            this.user.projects = [];
            this.user.departments = [];
            this.user.wards = [];


            this.selectedDepartmentOptions.forEach(el => {

                var dept = new Department();
                dept.department_id = el;
                this.user.departments.push(dept);
            });

            this.selectedProjectOptions.forEach(el => {
                var project = new Project();
                project.project_id_int = el;
                this.user.projects.push(project);
            });

            this.selectedWardOptions.forEach(el => {
                var ward = new Ward();
                ward.ward_id_int = el;
                this.user.wards.push(ward);
            });


            this.submit.emit(this.user);
            this.user = null;


        }
    }

    delete(): void {
        if (confirm("Sind Sie sich sicher, dass Sie den Benutzer " + this.user.user.user_name + " löschen wollen und alle zugehörigen Verknüpfungen?")) {

            var close = new ModalClose();
            close.Type = ModalCancelReason.DELETE;
            close.Id = this.user.user.user_id;

            this.cancel.emit(close);
            this.user = null;
        }

    }

    cancelForm(): void {
        var close = new ModalClose();
        close.Type = ModalCancelReason.CANCEL;

        this.cancel.emit(close);
        this.user = null;
    }



    departmentSelectionChanged(): void {

        // Deselektiertes Item finden
        var missingID = this.findDeselectedItem(this.oldDepartmentSelection, this.selectedDepartmentOptions);


        // Wenn es eins gibt, dann die Stationen finden und aus der Stationsselektion entfernen!
        if (missingID != null) {
            var wards = this.departmentWithWards.find(f => f.department_id === missingID).wards;

            wards.forEach(el => {
                let index = this.selectedWardOptions.indexOf(el.ward_id_int);
                if (index > -1) {
                    this.selectedWardOptions.splice(index, 1);
                }
            })

        }

        // zuerst sämtliche Klassen entfernen
        this.wardOptions.forEach(el => el.classes = '');


        this.selectedDepartmentOptions.forEach(el => {

            this.departmentWithWards.forEach(e => {
                if (e.department_id === el) {
                    // ausgewählter department 
                    // stationen in das stations array packen

                    e.wards.forEach(ward => {
                        // vorher prüfen ob die ward bereits ausgewählt ist

                        this.wardOptions.find(f => f.id === ward.ward_id_int).classes = 'departmentSelection';


                        if (!this.selectedWardOptions.some(wardEl => wardEl === ward.ward_id_int))
                            this.selectedWardOptions.push(ward.ward_id_int);
                    })
                }
            })
        })


        this.oldDepartmentSelection = Object.assign(new Array<number>(), this.selectedDepartmentOptions);
    }

    private findDeselectedItem(oldArray: number[], newArray: number[]): number {


        for (var i = 0; i < oldArray.length; i++) {
            if (newArray.indexOf(oldArray[i]) === -1)
                return oldArray[i];
        }

        return null;
    }

}