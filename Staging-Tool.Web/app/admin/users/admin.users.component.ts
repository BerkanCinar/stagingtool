﻿import { Component } from '@angular/core';
import { AdminService } from '../admin.service';
import { NotificationService } from '../../notification/notification.service';
import { Notification, NotificationType, UserRight, DetailState, ModalClose, ModalCancelReason } from '../../model/model';

@Component({
    moduleId: module.id.toString(),
    selector: 'app-admin-users',
    templateUrl: './admin.users.component.html',
    styleUrls: ['./admin.users.component.css']
})

export class AdminUsersComponent {


    private users: UserRight[];
    private newUser: UserRight;

    private selectedUser: UserRight;
    DetailState: typeof DetailState = DetailState;
    private selectedDetailState: DetailState;
    private expandedView: boolean = false;


    constructor(private adminService: AdminService, private notificationService: NotificationService) {

        this.refresh();
    }

    refresh(): void {
        this.adminService.getUsers().then(users => this.users = users);
    }

    setDetailState(state: DetailState): void {
        this.selectedDetailState = state;
    }

    addUser(): void {
        this.newUser = new UserRight();
    }



    // Modal functions

    delete(userId: number): void {
        this.adminService.deleteUser(userId).then(success => {
            if (success) {
                this.notificationService.note.next(new Notification("Benutzer erfolgreich gelöscht", NotificationType.SUCCESS));
                this.refresh();
            } else {
                this.notificationService.note.next(new Notification("Benutzer konnte nicht gelöscht werden", NotificationType.ERROR));
            }
        })
    }


    submitted(user: UserRight): void {
        this.adminService.addUser(user).then(success => {
            if (success) {
                this.notificationService.note.next(new Notification("Benutzer erfolgreich angelegt", NotificationType.SUCCESS));
                this.refresh();
            } else {
                this.notificationService.note.next(new Notification("Benutzer konnte nicht angelegt werden", NotificationType.ERROR));
            }
        })
    }

    resetPassword(userId: number): void {
        this.adminService.resetPassword(userId).then(success => {
            if (success) {
                this.notificationService.note.next(new Notification("Passwort erfolgreich Zurückgesetzt", NotificationType.SUCCESS));
            } else {
                this.notificationService.note.next(new Notification("Passwort zurücksetzten Fehlgeschlagen", NotificationType.ERROR));
            }
        })
    }

    canceled(obj: ModalClose): void {
        this.newUser = null;

        switch (obj.Type) {
            case ModalCancelReason.DELETE: this.delete(obj.Id);
                break;
            case ModalCancelReason.RESET: this.resetPassword(obj.Id);
                break;
        }
    }

}