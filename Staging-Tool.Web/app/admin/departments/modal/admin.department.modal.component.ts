﻿import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { UserRight, Department, Ward, ModalCancelReason, ModalClose } from '../../../model/model';

import { IMultiSelectOption, IMultiSelectSettings, IMultiSelectTexts } from 'angular-2-dropdown-multiselect';
import { AdminService } from '../../admin.service';



@Component({
    moduleId: module.id.toString(),
    selector: 'app-admin-department-modal',
    templateUrl: './admin.department.modal.component.html',
    styleUrls: ['../admin.department.component.css']
})

export class AdminDepartmentComponentModal implements OnInit {

    private dept: Department;
    @Input() set newDept(value: Department) {

        if (value) {


            this.dept = value;

            this.selectedWardOptions = [];

            this.dept.wards.forEach(el => {
                this.selectedWardOptions.push(el.ward_id_int);
            });
        }

    }

    //@Input() dept: Department;
    @Output() submit = new EventEmitter<Department>();
    @Output() cancel = new EventEmitter<ModalClose>();

    // Permissions
    private wardOptions: IMultiSelectOption[];
    private selectedWardOptions: number[];

    // Settings configuration
    private selectboxOptions: IMultiSelectSettings;

    // Text configuration
    private selectboxText: IMultiSelectTexts;

    ngOnInit() {
        this.selectboxOptions = {
            enableSearch: true,
            checkedStyle: 'fontawesome',
            buttonClasses: 'btn btn-default btn-block',
            dynamicTitleMaxItems: 0,
            displayAllSelectedText: true,
            itemClasses: 'dropdownItem'
        };

        this.selectboxText = {
            checkAll: 'Alle',
            uncheckAll: 'Keine',
            checked: 'Element ausgewählt',
            checkedPlural: 'Elemente ausgewählt',
            defaultTitle: 'Auswählen',
            allSelected: 'Alle selektiert',
            searchPlaceholder: 'Suche...'

        };

    }

    constructor(private adminService: AdminService) {
        adminService.getPermissions().then(data => {
            this.wardOptions = [];
            data.wards.forEach(el => {
                let name = el.ward_name ? el.ward_name : ''
                    + el.ward_short_name ? (" " + el.ward_short_name) : '';

                this.wardOptions.push({ id: el.ward_id_int, name: name }); // el.ward_name + " " + el.ward_short_name
            })
        });
    }


    addDepartment(): void {
        // object wieder zusammen basteln
        if (this.dept.department_name) {

            this.dept.wards = [];

            this.selectedWardOptions.forEach(el => {
                var ward = new Ward();
                ward.ward_id_int = el;
                this.dept.wards.push(ward);
            });


            this.submit.emit(this.dept);
            this.dept = null;


        }
    }

    delete(): void {
        if (confirm("Sind Sie sich sicher, dass Sie die Fachabteilung " + this.dept.department_short_name + " " + this.dept.department_name + " löschen wollen und alle zugehörigen Verknüpfungen?")) {

            var close = new ModalClose();
            close.Type = ModalCancelReason.DELETE;
            close.Id = this.dept.department_id;

            this.cancel.emit(close);
            this.dept = null;
        }

    }

    cancelForm(): void {
        var close = new ModalClose();
        close.Type = ModalCancelReason.CANCEL;

        this.cancel.emit(close);
        this.dept = null;
    }

}