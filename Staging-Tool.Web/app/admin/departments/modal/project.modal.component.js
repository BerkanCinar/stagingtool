"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var model_1 = require("../../model/model");
var ProjectComponentModal = (function () {
    function ProjectComponentModal() {
        this.submit = new core_1.EventEmitter();
        this.deleted = new core_1.EventEmitter();
    }
    ProjectComponentModal.prototype.addProject = function () {
        this.submit.emit(this.project);
        this.project = null;
    };
    ProjectComponentModal.prototype.delete = function () {
        this.deleted.emit(this.project.project_id_int);
        this.project = null;
    };
    return ProjectComponentModal;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", model_1.Project)
], ProjectComponentModal.prototype, "project", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ProjectComponentModal.prototype, "submit", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], ProjectComponentModal.prototype, "deleted", void 0);
ProjectComponentModal = __decorate([
    core_1.Component({
        moduleId: module.id.toString(),
        selector: 'app-project-modal',
        templateUrl: './project.modal.component.html',
        styleUrls: ['../project.component.css']
    }),
    __metadata("design:paramtypes", [])
], ProjectComponentModal);
exports.ProjectComponentModal = ProjectComponentModal;
//# sourceMappingURL=project.modal.component.js.map