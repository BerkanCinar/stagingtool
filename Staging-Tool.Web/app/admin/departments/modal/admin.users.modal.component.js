"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var model_1 = require("../../../model/model");
var admin_users_service_1 = require("../admin.users.service");
var AdminUsersComponentModal = (function () {
    function AdminUsersComponentModal(adminService) {
        var _this = this;
        this.adminService = adminService;
        //@Input() user: UserRight;
        this.submit = new core_1.EventEmitter();
        //@Output() deleted = new EventEmitter<number>();
        this.cancel = new core_1.EventEmitter();
        adminService.getPermissions().then(function (permission) {
            _this.departmentOptions = [];
            _this.projectOptions = [];
            _this.wardOptions = [];
            _this.selectedWardOptions = [];
            _this.selectedProjectOptions = [];
            _this.selectedDepartmentOptions = [];
            permission.departments.forEach(function (el) {
                return _this.departmentOptions.push({ id: el.department_id, name: el.department_name });
            });
            permission.projects.forEach(function (el) {
                return _this.projectOptions.push({ id: el.project_id_int, name: el.project_name });
            });
            permission.wards.forEach(function (el) {
                return _this.wardOptions.push({ id: el.ward_id, name: el.ward_name });
            });
        });
    }
    Object.defineProperty(AdminUsersComponentModal.prototype, "newUser", {
        set: function (value) {
            var _this = this;
            if (value) {
                if (value.user.user_valid_from) {
                    value.user.user_valid_from = new Date(value.user.user_valid_from);
                }
                if (value.user.user_valid_to) {
                    value.user.user_valid_to = new Date(value.user.user_valid_to);
                }
                this.user = value;
                this.selectedWardOptions = [];
                this.selectedProjectOptions = [];
                this.selectedDepartmentOptions = [];
                this.user.departments.forEach(function (el) {
                    _this.selectedDepartmentOptions.push(el.department_id);
                });
                this.user.projects.forEach(function (el) {
                    _this.selectedProjectOptions.push(el.project_id_int);
                });
                this.user.wards.forEach(function (el) {
                    _this.selectedWardOptions.push(el.ward_id);
                });
            }
        },
        enumerable: true,
        configurable: true
    });
    AdminUsersComponentModal.prototype.ngOnInit = function () {
        this.de = {
            firstDayOfWeek: 1,
            dayNames: ["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"],
            dayNamesShort: ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"],
            dayNamesMin: ["M", "D", "M", "D", "F", "S", "S"],
            monthNames: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
            monthNamesShort: ["Jan", "Feb", "März", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
            today: 'Heute',
            clear: 'Zurücksetzten'
        };
        this.selectboxOptions = {
            checkedStyle: 'fontawesome',
            buttonClasses: 'btn btn-default btn-block',
            dynamicTitleMaxItems: 0,
            displayAllSelectedText: true,
            itemClasses: 'dropdownItem'
        };
        this.selectboxText = {
            checkAll: 'Alle',
            uncheckAll: 'Keine',
            checked: 'Element ausgewählt',
            checkedPlural: 'Elemente ausgewählt',
            defaultTitle: 'Auswählen',
            allSelected: 'Alle selektiert'
        };
    };
    AdminUsersComponentModal.prototype.resetPasswort = function () {
        if (confirm("Sind Sie sich sicher, dass Sie das Passwort vom Benutzer " + this.user.user.user_name + " zurücksetzten wollen?")) {
            var close = new model_1.ModalClose();
            close.Type = model_1.ModalCancelReason.RESET;
            close.Id = this.user.user.user_id;
            this.cancel.emit(close);
            this.user = null;
        }
    };
    AdminUsersComponentModal.prototype.addUser = function () {
        var _this = this;
        // object wieder zusammen basteln
        if (this.user.user.user_valid_from && this.user.user.user_name) {
            this.user.projects = [];
            this.user.departments = [];
            this.user.wards = [];
            this.selectedDepartmentOptions.forEach(function (el) {
                var dept = new model_1.Department();
                dept.department_id = el;
                _this.user.departments.push(dept);
            });
            this.selectedProjectOptions.forEach(function (el) {
                var project = new model_1.Project();
                project.project_id_int = el;
                _this.user.projects.push(project);
            });
            this.selectedWardOptions.forEach(function (el) {
                var ward = new model_1.Ward();
                ward.ward_id = el;
                _this.user.wards.push(ward);
            });
            this.submit.emit(this.user);
            this.user = null;
        }
    };
    AdminUsersComponentModal.prototype.delete = function () {
        if (confirm("Sind Sie sich sicher, dass Sie den Benutzer " + this.user.user.user_name + " löschen wollen und alle zugehörigen Verknüpfungen?")) {
            var close = new model_1.ModalClose();
            close.Type = model_1.ModalCancelReason.DELETE;
            close.Id = this.user.user.user_id;
            this.cancel.emit(close);
            this.user = null;
        }
    };
    AdminUsersComponentModal.prototype.cancelForm = function () {
        var close = new model_1.ModalClose();
        close.Type = model_1.ModalCancelReason.CANCEL;
        this.cancel.emit(close);
        this.user = null;
    };
    return AdminUsersComponentModal;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", model_1.UserRight),
    __metadata("design:paramtypes", [model_1.UserRight])
], AdminUsersComponentModal.prototype, "newUser", null);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], AdminUsersComponentModal.prototype, "submit", void 0);
__decorate([
    core_1.Output(),
    __metadata("design:type", Object)
], AdminUsersComponentModal.prototype, "cancel", void 0);
AdminUsersComponentModal = __decorate([
    core_1.Component({
        moduleId: module.id.toString(),
        selector: 'app-admin-users-modal',
        templateUrl: './admin.users.modal.component.html',
        styleUrls: ['../admin.users.component.css']
    }),
    __metadata("design:paramtypes", [admin_users_service_1.AdminUsersService])
], AdminUsersComponentModal);
exports.AdminUsersComponentModal = AdminUsersComponentModal;
//# sourceMappingURL=admin.users.modal.component.js.map