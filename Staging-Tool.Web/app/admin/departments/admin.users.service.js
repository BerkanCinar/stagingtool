"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var httpclient_service_1 = require("../../http/httpclient.service");
var AdminUsersService = (function () {
    function AdminUsersService(httpservice) {
        this.httpservice = httpservice;
    }
    AdminUsersService.prototype.getUsers = function () {
        return this.httpservice.get("api/User")
            .then(function (response) {
            return JSON.parse(response['_body']);
        })
            .catch(function (error) {
            return [];
        });
    };
    AdminUsersService.prototype.getPermissions = function () {
        return this.httpservice.get("api/Permission")
            .then(function (response) {
            return JSON.parse(response['_body']);
        })
            .catch(function (error) {
            return null;
        });
    };
    AdminUsersService.prototype.deleteUser = function (userId) {
        return this.httpservice.delete("api/User", userId)
            .then(function (response) {
            return true;
        })
            .catch(function (error) {
            return false;
        });
    };
    AdminUsersService.prototype.addUser = function (user) {
        return this.httpservice.post("api/User", { user: user })
            .then(function (response) {
            return true;
        })
            .catch(function (error) {
            return false;
        });
    };
    AdminUsersService.prototype.resetPassword = function (userId) {
        return this.httpservice.putWithId("api/User/PasswordReset", userId)
            .then(function (response) {
            return true;
        })
            .catch(function (error) {
            return false;
        });
    };
    return AdminUsersService;
}());
AdminUsersService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [httpclient_service_1.HttpClientService])
], AdminUsersService);
exports.AdminUsersService = AdminUsersService;
//# sourceMappingURL=admin.users.service.js.map