﻿import { Component } from '@angular/core';
import { AdminService } from '../admin.service';
import { NotificationService } from '../../notification/notification.service';
import { Notification, NotificationType, Department, Ward, ModalClose, ModalCancelReason } from '../../model/model';

@Component({
    moduleId: module.id.toString(),
    selector: 'app-admin-departments',
    templateUrl: './admin.department.component.html',
    styleUrls: ['./admin.department.component.css']
})

export class AdminDepartmentComponent {


    private depts: Department[];
    private newDept: Department;


    constructor(private adminService: AdminService, private notificationService: NotificationService) {
        this.refresh();
    }

    refresh(): void {
        this.adminService.getDepartmentData().then(data => {
            this.depts = data.departments;
        });
    }

    addDepartment(): void {
        this.newDept = new Department();
    }



    // Modal functions

    delete(deptId: number): void {
        this.adminService.deleteDepartment(deptId).then(success => {
            if (success) {
                this.notificationService.note.next(new Notification("Fachabteilung erfolgreich gelöscht", NotificationType.SUCCESS));
                this.refresh();
            } else {
                this.notificationService.note.next(new Notification("Fachabteilung konnte nicht gelöscht werden", NotificationType.ERROR));
            }
        })
    }


    submitted(dept: Department): void {
        this.adminService.addDepartment(dept).then(success => {
            if (success) {
                this.notificationService.note.next(new Notification("Fachabteilung erfolgreich angelegt", NotificationType.SUCCESS));
                this.refresh();
            } else {
                this.notificationService.note.next(new Notification("Fachabteilung konnte nicht angelegt werden", NotificationType.ERROR));
            }
        })
    }

    canceled(obj: ModalClose): void {
        this.newDept = null;

        switch (obj.Type) {
            case ModalCancelReason.DELETE: this.delete(obj.Id);
                break;
        }
    }

}