"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var admin_users_service_1 = require("./admin.users.service");
var notification_service_1 = require("../../notification/notification.service");
var model_1 = require("../../model/model");
var AdminUsersComponent = (function () {
    function AdminUsersComponent(adminUsersService, notificationService) {
        this.adminUsersService = adminUsersService;
        this.notificationService = notificationService;
        this.DetailState = model_1.DetailState;
        this.expandedView = false;
        this.refresh();
    }
    AdminUsersComponent.prototype.refresh = function () {
        var _this = this;
        this.adminUsersService.getUsers().then(function (users) { return _this.users = users; });
    };
    AdminUsersComponent.prototype.setDetailState = function (state) {
        this.selectedDetailState = state;
        console.log(this.selectedDetailState);
    };
    AdminUsersComponent.prototype.addUser = function () {
        this.newUser = new model_1.UserRight();
    };
    // Modal functions
    AdminUsersComponent.prototype.delete = function (userId) {
        var _this = this;
        this.adminUsersService.deleteUser(userId).then(function (success) {
            if (success) {
                _this.notificationService.note.next(new model_1.Notification("Benutzer erfolgreich gelöscht", model_1.NotificationType.SUCCESS));
                _this.refresh();
            }
            else {
                _this.notificationService.note.next(new model_1.Notification("Benutzer konnte nicht gelöscht werden", model_1.NotificationType.ERROR));
            }
        });
    };
    AdminUsersComponent.prototype.submitted = function (user) {
        var _this = this;
        this.adminUsersService.addUser(user).then(function (success) {
            if (success) {
                _this.notificationService.note.next(new model_1.Notification("Benutzer erfolgreich angelegt", model_1.NotificationType.SUCCESS));
                _this.refresh();
            }
            else {
                _this.notificationService.note.next(new model_1.Notification("Benutzer konnte nicht angelegt werden", model_1.NotificationType.ERROR));
            }
        });
    };
    AdminUsersComponent.prototype.resetPassword = function (userId) {
        var _this = this;
        this.adminUsersService.resetPassword(userId).then(function (success) {
            if (success) {
                _this.notificationService.note.next(new model_1.Notification("Passwort erfolgreich Zurückgesetzt", model_1.NotificationType.SUCCESS));
            }
            else {
                _this.notificationService.note.next(new model_1.Notification("Passwort zurücksetzten Fehlgeschlagen", model_1.NotificationType.ERROR));
            }
        });
    };
    AdminUsersComponent.prototype.canceled = function (obj) {
        this.newUser = null;
        switch (obj.Type) {
            case model_1.ModalCancelReason.DELETE:
                this.delete(obj.Id);
                break;
            case model_1.ModalCancelReason.RESET:
                this.resetPassword(obj.Id);
                break;
        }
    };
    return AdminUsersComponent;
}());
AdminUsersComponent = __decorate([
    core_1.Component({
        moduleId: module.id.toString(),
        selector: 'app-admin-users',
        templateUrl: './admin.users.component.html',
        styleUrls: ['./admin.users.component.css']
    }),
    __metadata("design:paramtypes", [admin_users_service_1.AdminUsersService, notification_service_1.NotificationService])
], AdminUsersComponent);
exports.AdminUsersComponent = AdminUsersComponent;
//# sourceMappingURL=admin.users.component.js.map