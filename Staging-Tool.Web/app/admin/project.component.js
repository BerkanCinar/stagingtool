"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var project_service_1 = require("./project.service");
var notification_service_1 = require("../notification/notification.service");
var app_service_1 = require("../app.service");
require("rxjs/Rx");
var model_1 = require("../model/model");
var ProjectComponent = (function () {
    function ProjectComponent(projectService, globalService, notificationService) {
        this.projectService = projectService;
        this.globalService = globalService;
        this.notificationService = notificationService;
        this.refresh();
    }
    ProjectComponent.prototype.refresh = function () {
        var _this = this;
        this.projectService.getProjects().then(function (projects) {
            _this.projects = projects;
        });
    };
    ProjectComponent.prototype.deleted = function (projectId) {
        var _this = this;
        alert(projectId);
        this.projectService.removeProject(projectId).then(function (success) {
            if (success) {
                _this.notificationService.note.next(new model_1.Notification("Projekt gelöscht!", model_1.NotificationType.SUCCESS));
                _this.refresh();
            }
            else {
                _this.notificationService.note.next(new model_1.Notification("Projekt konnte nicht gelöscht werden! Evtl. sind bereits Patienten in dem Projekt", model_1.NotificationType.ERROR));
            }
        });
    };
    ProjectComponent.prototype.selectProject = function (project) {
        this.project = Object.assign({}, project); // Copy Object to lose the object reference!
    };
    ProjectComponent.prototype.submitted = function (project) {
        var _this = this;
        this.projectService.getRedCapProject(project.token).then(function (proj) {
            if (proj) {
                var stagingProject = new model_1.Project();
                stagingProject.token = project.token;
                stagingProject.project_name = proj.project_title;
                stagingProject.project_id_ext = proj.project_id;
                _this.projectService.addNewProject(stagingProject).then(function (success) {
                    if (success) {
                        _this.notificationService.note.next(new model_1.Notification("Projekt hinzugefügt!", model_1.NotificationType.SUCCESS));
                        _this.refresh();
                    }
                    else {
                        _this.notificationService.note.next(new model_1.Notification("Projekt konnte nicht hinzugefügt werden!!", model_1.NotificationType.ERROR));
                    }
                });
            }
            else {
                _this.notificationService.note.next(new model_1.Notification("Projekt-Token nicht vorhanden!", model_1.NotificationType.ERROR));
            }
        });
    };
    ProjectComponent.prototype.addProject = function () {
        this.project = new model_1.Project();
    };
    return ProjectComponent;
}());
ProjectComponent = __decorate([
    core_1.Component({
        moduleId: module.id.toString(),
        selector: 'app-project',
        templateUrl: './project.component.html',
        styleUrls: ['./project.component.css']
    }),
    __metadata("design:paramtypes", [project_service_1.ProjectService, app_service_1.AppService, notification_service_1.NotificationService])
], ProjectComponent);
exports.ProjectComponent = ProjectComponent;
//# sourceMappingURL=project.component.js.map