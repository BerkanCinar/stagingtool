"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var httpclient_service_1 = require("../http/httpclient.service");
var ProjectService = (function () {
    function ProjectService(httpservice) {
        this.httpservice = httpservice;
    }
    // Overview
    ProjectService.prototype.getProjects = function () {
        return this.httpservice.get("api/Project")
            .then(function (response) {
            return JSON.parse(response['_body']);
        })
            .catch(function (error) {
            return [];
        });
    };
    ProjectService.prototype.addNewProject = function (project) {
        return this.httpservice.post("api/Project", { ProjectObj: project })
            .then(function (response) {
            return true;
        })
            .catch(function (error) {
            return false;
        });
    };
    ProjectService.prototype.getRedCapProject = function (token) {
        var data = "token=" + token + "&content=project&format=json";
        var api = "http://hosting3924.af927.netcup.net/redcap/api/";
        var fhApi = "http://fb5-web.fh-bielefeld.de/~stud335/redcap/api/";
        return this.httpservice.postRedCap(api, data)
            .then(function (response) {
            return JSON.parse(response['_body']);
        })
            .catch(function (error) {
            return null;
        });
    };
    ProjectService.prototype.removeProject = function (projectId) {
        return this.httpservice.delete("api/Project", projectId)
            .then(function (response) {
            return true;
        })
            .catch(function (error) {
            return false;
        });
    };
    return ProjectService;
}());
ProjectService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [httpclient_service_1.HttpClientService])
], ProjectService);
exports.ProjectService = ProjectService;
//# sourceMappingURL=project.service.js.map