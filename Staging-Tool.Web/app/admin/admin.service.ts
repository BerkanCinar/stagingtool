﻿import { Injectable } from '@angular/core';
import { HttpClientService } from '../http/httpclient.service';
import { UserRight, Permission, Department, DepartmentData } from '../model/model';

@Injectable()
export class AdminService {

    constructor(private httpservice: HttpClientService) {
    }


    getUsers(): Promise<UserRight[]> {
        return this.httpservice.get("api/User")
            .then(response => {
                return JSON.parse(response['_body']) as UserRight[];
            })
            .catch(error => {
                return [];
            });
    }

    getPermissions(): Promise<Permission> {
        return this.httpservice.get("api/Permission")
            .then(response => {
                return JSON.parse(response['_body']) as Permission;
            })
            .catch(error => {
                return null;
            });
    }



    deleteUser(userId: number): Promise<boolean> {
        return this.httpservice.delete("api/User", userId)
            .then(response => {
                return true;
            })
            .catch(error => {
                return false;
            });
    }

    addUser(user: UserRight): Promise<boolean> {
        return this.httpservice.post("api/User", { user: user })
            .then(response => {
                return true;
            })
            .catch(error => {
                return false;
            });
    }

    resetPassword(userId: number): Promise<boolean> {
        return this.httpservice.putWithId("api/User/PasswordReset", userId)
            .then(response => {
                return true;
            })
            .catch(error => {
                return false;
            });
    }

    // Department 
    getDepartmentData(): Promise<DepartmentData> {
        return this.httpservice.get("api/Department")
            .then(response => {
                return JSON.parse(response['_body']) as DepartmentData;
            })
            .catch(error => {
                return new DepartmentData();
            });
    }


    addDepartment(dept: Department): Promise<boolean> {
        return this.httpservice.post("api/Department", { department: dept })
            .then(response => {
                return true;
            })
            .catch(error => {
                return false;
            });
    }

    deleteDepartment(deptId: number): Promise<boolean> {
        return this.httpservice.delete("api/Department", deptId)
            .then(response => {
                return true;
            })
            .catch(error => {
                return false;
            });
    }

}