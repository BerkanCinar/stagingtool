﻿import { Component } from '@angular/core';
import { UserRight, AdminState } from '../model/model';

@Component({
    moduleId: module.id.toString(),
    selector: 'app-admin',
    templateUrl: './admin.component.html',
    styleUrls: ['./admin.component.css']
})

export class AdminComponent {

    adminState: typeof AdminState = AdminState;
    selectedState: AdminState;

    constructor() {
        this.selectedState = AdminState.USERS;
    }

}