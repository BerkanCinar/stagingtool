﻿import { Component, OnInit } from '@angular/core';
import { LoginService } from './login.service';
import { Http, Response } from '@angular/http';
import { NotificationService } from '../notification/notification.service';
import { AppService } from '../app.service';
import { Router } from '@angular/router';
import { Notification, NotificationType, User } from '../model/model';


@Component({
    moduleId: module.id.toString(),
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.css']
})

export class LoginComponent {

    private user: User;
    private initUser: User;


    constructor(private loginService: LoginService, private globalService: AppService, private notificationService: NotificationService, private router: Router) {
        this.user = new User();

    }

    passwordChanged(): void {
        this.redirect();
    }

    redirect(): void {
        this.globalService.loginSource.next(true);
        this.router.navigate(["/patient"]);
    }

    login(): void {

        if (this.user.user_password && this.user.user_name) {


            this.loginService.login(this.user).then(success => {
                if (success) {

                    // get User first of all
                    this.loginService.getUser(this.user.user_name).then(user => {
                        // redirect
                        this.globalService.user.next(user);

                        if (user.initial_password) {
                            this.initUser = Object.assign({}, user);
                        } else {
                            this.redirect();
                        }

                    });
                } else {
                    this.notificationService.note.next(new Notification("Benutzername oder Password falsch!", NotificationType.ERROR));
                }
            })
        }
    }


}