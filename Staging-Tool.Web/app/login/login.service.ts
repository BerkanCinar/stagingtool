﻿import { Injectable } from '@angular/core';
import { HttpClientService } from '../http/httpclient.service';
import { User } from '../model/model';

@Injectable()
export class LoginService {

    constructor(private httpservice: HttpClientService) {
    }



    login(user: User): Promise<boolean> {


        let data = {
            "UserName": user.user_name,
            "Password": btoa(user.user_name + ":" + user.user_password),
            "RememberMe": true
        };

        return this.httpservice.post("api/auth", data)
            .then(response => {
                this.setSession((JSON.parse(response['_body'])).SessionId);
                return true;
            })
            .catch(error => {
                return false;
            });

    }

    getUser(userName: string): Promise<User> {
        return this.httpservice.get("api/User/Name/" + userName)
            .then(response => {
                return JSON.parse(response['_body']) as User[];
            })
            .catch(error => {
                return null;
            });

    }



    setSession(id: any): void {
        sessionStorage.setItem('sessionId', id);
    }


    changePassword(user: User): Promise<boolean> {
        return this.httpservice.put("api/User/ChangePassword", { changePasswordObject: user })
            .then(response => {
                return true;
            })
            .catch(error => {
                return false;
            });

    }


}