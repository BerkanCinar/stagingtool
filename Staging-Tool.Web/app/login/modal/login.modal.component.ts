﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import { User } from '../../model/model';
import { AppService } from '../../app.service';
 

import { LoginService } from '../login.service';


@Component({
    moduleId: module.id.toString(),
    selector: 'app-login-modal',
    templateUrl: './login.modal.component.html',
    styleUrls: ['../login.component.css']
})

export class LoginComponentModal {

    private passwordRepeat: string;
    private password: string;
    private error: string;

    @Input() user: User;
    @Output() changed = new EventEmitter();
   

    constructor(private loginService: LoginService, private logoutService: AppService) {
    }


    changePassword(): void {
        this.error = null;
        if (this.passwordRepeat === this.password) {

            this.user.user_password = btoa(this.user.user_name + ":" + this.password);
        
            this.loginService.changePassword(this.user).then(success => {
                if (success) {
                    this.changed.emit();
                } else {
                    this.error = "Fehler beim setzten des Passworts";
                }
            })
        } else {
            this.error = "Passwörter stimmen nicht überein";
        }
    }

    cancelForm(): void {
        this.error = null;
        this.logoutService.logout().then(success => {
            if (success) {
                this.user = null;
            } else {
                this.error = "Fehler";
            }
        })
        
    }
    
}