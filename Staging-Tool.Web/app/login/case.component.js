"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var case_service_1 = require("./case.service");
var notification_service_1 = require("../notification/notification.service");
var app_service_1 = require("../app.service");
require("rxjs/Rx");
var model_1 = require("../model/model");
var CaseComponent = (function () {
    function CaseComponent(caseService, globalService, notificationService) {
        this.caseService = caseService;
        this.globalService = globalService;
        this.notificationService = notificationService;
        this.searchPatient = new model_1.Patient();
        this.yearrange = "1900:" + (new Date()).getFullYear();
    }
    CaseComponent.prototype.ngOnInit = function () {
        this.de = {
            firstDayOfWeek: 1,
            dayNames: ["Montag", "Dienstag", "Mittwoch", "Donnerstag", "Freitag", "Samstag", "Sonntag"],
            dayNamesShort: ["Mo", "Di", "Mi", "Do", "Fr", "Sa", "So"],
            dayNamesMin: ["M", "D", "M", "D", "F", "S", "S"],
            monthNames: ["Januar", "Februar", "März", "April", "Mai", "Juni", "Juli", "August", "September", "Oktober", "November", "Dezember"],
            monthNamesShort: ["Jan", "Feb", "März", "Apr", "Mai", "Jun", "Jul", "Aug", "Sep", "Okt", "Nov", "Dez"],
            today: 'Heute',
            clear: 'Zurücksetzten'
        };
    };
    CaseComponent.prototype.search = function () {
        var _this = this;
        this.caseService.searchPatients(this.searchPatient).then(function (patients) {
            _this.patients = patients;
            if (_this.patients.length === 1) {
                _this.selectedPatient = _this.patients[0];
            }
        });
    };
    CaseComponent.prototype.addCase = function () {
        alert('adding');
    };
    return CaseComponent;
}());
CaseComponent = __decorate([
    core_1.Component({
        moduleId: module.id.toString(),
        selector: 'app-case',
        templateUrl: './case.component.html',
        styleUrls: ['./case.component.css']
    }),
    __metadata("design:paramtypes", [case_service_1.CaseService, app_service_1.AppService, notification_service_1.NotificationService])
], CaseComponent);
exports.CaseComponent = CaseComponent;
//# sourceMappingURL=case.component.js.map