"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
var core_1 = require("@angular/core");
var httpclient_service_1 = require("../http/httpclient.service");
var CaseService = (function () {
    function CaseService(httpservice) {
        this.httpservice = httpservice;
    }
    CaseService.prototype.searchPatients = function (patient) {
        return this.httpservice.post("api/SearchPatient", { PatientObj: patient })
            .then(function (response) {
            return JSON.parse(response['_body']);
        })
            .catch(function (error) {
            return [];
        });
    };
    return CaseService;
}());
CaseService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [httpclient_service_1.HttpClientService])
], CaseService);
exports.CaseService = CaseService;
//# sourceMappingURL=case.service.js.map