﻿import { Component } from '@angular/core';
import { NotificationService } from './notification.service';
import { Http, Response } from '@angular/http';
import 'rxjs/Rx';
import { NotificationType, Notification } from '../model/model';

@Component({
    moduleId: module.id.toString(),
    selector: 'app-notification',
    templateUrl: './notification.component.html',
    styleUrls: ['./notification.component.css']
})

export class NotificationComponent {

    private notification: Notification;
    private timerID: any;
    notificationType: typeof NotificationType = NotificationType;

    constructor(private notificationService: NotificationService) {
        notificationService.note.subscribe((note: Notification) => {
            this.notification = note;
            clearTimeout(this.timerID);
            this.timerID = setTimeout(() => this.notification = null, 5000);
        });
    }
}
