﻿import { Injectable } from '@angular/core';
import { Notification } from '../model/model';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class NotificationService {
    note: Subject<Notification> = new Subject();

}
    