﻿import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';

@Injectable()
export class AuthGuard implements CanActivate {
    canActivate() {
        var id = sessionStorage.getItem('sessionId');
        if (id)
            return true
        return false;
    }
}