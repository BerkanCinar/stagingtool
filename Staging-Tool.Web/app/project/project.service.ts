﻿import { Injectable } from '@angular/core';
import { HttpClientService } from '../http/httpclient.service';
import { Project, RedCapProject } from '../model/model';

@Injectable()
export class ProjectService {

    constructor(private httpservice: HttpClientService) {
    }

    // Overview
    getProjects(): Promise<Project[]> {
        return this.httpservice.get("api/Project")
            .then(response => {
                return JSON.parse(response['_body']) as Project[];
            })
            .catch(error => {
                return [];
            });
    }

    addNewProject(project: Project): Promise<boolean> {
        return this.httpservice.post("api/Project", { ProjectObj: project })
            .then(response => {
                return true;
            })
            .catch(error => {
                return false;
            });
    }

    removeProject(projectId: number): Promise<boolean> {

        return this.httpservice.delete("api/Project", projectId)
            .then(response => {
                return true
            })
            .catch(error => {
                return false;
            });
    }
}