﻿import { Component } from '@angular/core';
import { ProjectService } from './project.service';
import { Http, Response } from '@angular/http';
import { NotificationService } from '../notification/notification.service';
import { Notification, NotificationType, Project, RedCapProject } from '../model/model';

@Component({
    moduleId: module.id.toString(),
    selector: 'app-project',
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.css']
})

export class ProjectComponent {

    private projects: Project[];
    private project: Project;

    constructor(private projectService: ProjectService, private notificationService: NotificationService) {
        this.refresh();
    }

    refresh(): void {
        this.projectService.getProjects().then(projects => {
            this.projects = projects;
        })
    }

    selectProject(project: Project) {
        this.project = Object.assign({}, project); // Copy Object to lose the object reference!
    }


    deleted(projectId: number): void {

        this.projectService.removeProject(projectId).then(success => {
            if (success) {
                this.notificationService.note.next(new Notification("Projekt gelöscht!", NotificationType.SUCCESS));
                this.refresh();
            } else {
                this.notificationService.note.next(new Notification("Projekt konnte nicht gelöscht werden! Evtl. sind bereits Patienten in dem Projekt", NotificationType.ERROR));
            }
        })
    }


    submitted(project: Project): void {

        this.projectService.addNewProject(project).then(success => {
            if (success) {
                this.notificationService.note.next(new Notification("Projekt hinzugefügt!", NotificationType.SUCCESS));
                this.refresh();
            } else {
                this.notificationService.note.next(new Notification("Projekt konnte nicht hinzugefügt werden!", NotificationType.ERROR));
            }
        });
    }


    addProject(): void {
        this.project = new Project();
    }
}