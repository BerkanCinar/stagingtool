﻿import { Component, Input, Output, EventEmitter } from '@angular/core';
import { Project } from '../../model/model';

@Component({
    moduleId: module.id.toString(),
    selector: 'app-project-modal',
    templateUrl: './project.modal.component.html',
    styleUrls: ['../project.component.css']
})

export class ProjectComponentModal {

    @Input() project: Project;
    @Output() submit = new EventEmitter<Project>();
    @Output() deleted = new EventEmitter<number>();

    constructor() { }

    addProject(): void {
        this.submit.emit(this.project);
        this.project = null;
    }

    delete(): void {
        this.deleted.emit(this.project.project_id_int);
        this.project = null;
    }
}