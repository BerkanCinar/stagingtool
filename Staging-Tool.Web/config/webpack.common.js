var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var helpers = require('./helpers');
var WebpackCleanupPlugin = require('webpack-cleanup-plugin');



module.exports = {
    entry: {
        'polyfills': './polyfills.ts',
        'vendor': './vendor.ts',
        'app': './main.ts'
    },

    resolve: {
        extensions: ['.ts', '.js']
    },

    module: {
        rules: [
          {
              test: /\.ts$/,
              use: ['awesome-typescript-loader', 'angular2-template-loader']
              //loaders: [{
              //    loader: 'awesome-typescript-loader',
              //    options: { configFileName: helpers.root('src', 'tsconfig.json') }
              //}, 'angular2-template-loader']
          },
          {
              test: /\.html$/,
              use: 'html-loader'
          },
          {
              test: /\.json$/,
              use: 'file-loader?name=[name].[ext]',
              include: helpers.rootWebProject('app'),
          },
          {
              test: /\.(png|jpe?g|gif||ico)(\?.*$|$)/,
              use: 'file-loader?name=assets/[name].[ext]'
          },
          {
              test: /\.(woff|woff2|ttf|eot|svg)(\?[\s\S]+)?$/,
              use: 'file-loader?name=assets/[name].[ext]&publicPath=./' // Workaround for Fontawesome imports
          },
          {
              test: /\.css$/,
              exclude: helpers.rootWebProject('app'),
              use: ExtractTextPlugin.extract({ fallback: 'style-loader', use: 'css-loader?sourceMap' })
          },
          {
              test: /\.css$/,
              include: helpers.rootWebProject('app'),
              use: 'raw-loader'
          }
        ]
    },

    plugins: [
        new WebpackCleanupPlugin(),
      // Workaround for angular/angular#11580
      new webpack.ContextReplacementPlugin(
        // The (\\|\/) piece accounts for path separators in *nix and Windows
        /angular(\\|\/)core(\\|\/)(esm(\\|\/)src|src)(\\|\/)linker/,
        helpers.rootWebProject(), // location of your src
        {} // a map of your routes
      ),

      new webpack.optimize.CommonsChunkPlugin({
          name: ['app', 'vendor', 'polyfills', 'model']
      }),


      new HtmlWebpackPlugin({
          template: 'index-build.html',
          filename: '../index.html'
      })
    ]
};
