var path = require('path');
var _root = path.resolve(__dirname, '../../Staging-Tool.Deploy');
function root(args) {
    args = Array.prototype.slice.call(arguments, 0);
    return path.join.apply(path, [_root].concat(args));
}

var _webProjectRoot = path.resolve(__dirname, '..');
function rootWebProject(args) {
    args = Array.prototype.slice.call(arguments, 0);
    return path.join.apply(path, [_webProjectRoot].concat(args));
}
exports.root = root;
exports.rootWebProject = rootWebProject;